package com.mycompany.proyectourgencias.vistas;
import  com.mycompany.proyectourgencias.entidades.TipoRol;

import com.mycompany.proyectourgencias.entidades.Usuario;
import com.mycompany.proyectourgencias.servicio.UsuarioServiceException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 * @author Ruben Juan
 */

public class Login {
    private SessionFactory sessionFactory;
    private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    
    public Login(){
        try{
                ServiceRegistry serviceRegistry;
                Configuration configuration = new Configuration();
                configuration.configure();
                serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
                
                System.out.println("--------------------------------");
                System.out.println("---------Iniciar Sesion---------");
                System.out.println("--------------------------------");
            } 
            catch (Throwable e) {
                    System.err.println("Initial SessionFactory creation failed." + e);
                    throw new ExceptionInInitializerError(e);
            }
    }
    
    public void iniciarSesion() throws Exception{
        int i = 0;
        Session sesion;
        List<Usuario> listaUsuarios;
        String consulta;
        
        try {
            sesion = sessionFactory.openSession();
            Transaction tx = sesion.beginTransaction();
            
            try {
                System.out.print("1. Logearse\n0. Salir\n\nOpcion: ");
                int op = Integer.parseInt(br.readLine());

                while (op != 0) {                
                    System.out.print("Usuario: ");
                    String usuario = br.readLine(); 

                    System.out.printf("Contraseña: ");
                    String pwd = br.readLine();

                    consulta = "FROM Usuario WHERE usu='" + usuario + "'" +
                               " and pwd='" + pwd + "'";

                    listaUsuarios = (List<Usuario>)sesion.createQuery(consulta).list();            

                    if (listaUsuarios.isEmpty()) {
                        System.out.println("\t--> El usuario o contraseña no existe");
                        System.out.println();
                        this.iniciarSesion();
                    }

                        for (Usuario usu : listaUsuarios) {
                            if (usu.getUsu().equals(usuario) && usu.getPwd().equals(pwd)) {
                               switch(usu.getRol()){
                                   case ADMINISTRATIVO:
                                       System.out.println();
                                       PrincipalAdministrativo pA = new PrincipalAdministrativo();
                                       pA.menu();
                                       break;
                                   case DIRECTIVO:
                                       System.out.println();
                                       PrincipalDirectivo pD = new PrincipalDirectivo();
                                       pD.menu();
                                       break;
                                   case SANITARIO:
                                       PrincipalSanitario pS = new PrincipalSanitario();
                                       pS.menu();
                                       break;
                               }
                            }
                        }
                    System.out.println();
                    System.out.print("1. Logearse\n0. Salir\n\nOpcion: ");
                    op = Integer.parseInt(br.readLine());
                }
            } catch (Exception e) {
                System.out.println("\n--> La opcion introducida no es correcta. Vuelve a intentarlo <--\n");
                this.iniciarSesion();
            }
            
            sesion.close();

        } catch (Exception e){
           throw new Exception("Error en Login: " + e.getLocalizedMessage());
        }
    }

}
