package com.mycompany.proyectourgencias.vistas;


import com.mycompany.proyectourgencias.datos.IncidenciaDAO;
import com.mycompany.proyectourgencias.datos.MedicoDAO;
import com.mycompany.proyectourgencias.datos.PacienteDAO;
import com.mycompany.proyectourgencias.entidades.Incidencia;
import com.mycompany.proyectourgencias.entidades.Medico;
import com.mycompany.proyectourgencias.entidades.Paciente;
import com.mycompany.proyectourgencias.entidades.Usuario;
import com.mycompany.proyectourgencias.servicio.MedicoService;
import com.mycompany.proyectourgencias.servicio.PacienteService;
import com.mycompany.proyectourgencias.servicio.UrgenciasService;
import com.mycompany.proyectourgencias.servicio.UsuarioService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * @author Ruben Juan
 */
public class PrincipalSanitario  {
    private static Date d = new Date();
    public static Random r = new Random();
    public static UrgenciasService uS=new UrgenciasService();
    public static PacienteService pS = new PacienteService();
    //public static PacienteDAO pD=new PacienteDAO();
    public static MedicoService mS = new MedicoService();
    //public static IncidenciaDAO idao = new IncidenciaDAO();
    //public static MedicoDAO mdao = new MedicoDAO();
    public static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    
    public PrincipalSanitario(){
        System.out.println("----------------------------------");
        System.out.println("-----MENU-PRINCIPAL-SANITARIO-----");
        System.out.println("----------------------------------");
    }
    
    public void menu() throws Exception{
        System.out.print("1. Nuevo Paciente\n"
                       + "2. Actualizar Paciente\n"
                       + "3. Eliminar Paciente\n"
                       + "4. Iniciar Turno Medico\n"
                       + "5. Medicos en activo\n"
                       + "6. Medicos disponibles\n"
                       + "7. Finalizar Turno Medico\n"
                       + "8. Nueva Incidencia\n"
                       + "9. Finalizar Incidencia\n"
                       +"10. Obtener Incidencias en Espera\n"
                       + "11. Obtener Incidencias Paciente\n"
                       + "12. Obtener Pacientes en Espera\n"
                       + "0. Salir\n"
                       + "Introduce una opción: ");
        try {
            int op = Integer.parseInt(br.readLine());
            System.out.println();
            while (op != 0) {            
                switch(op){
                    case 1: // Nuevo Paciente
                        this.nuevoPaciente();
                        break;
                    case 2: // Actualizar Paciente
                        this.modificarPaciente();
                        break;
                    case 3: // Eliminar Paciente
                        this.eliminarPaciente();
                        break;
                    case 4: // Iniciar turno de Medico
                        this.iniciarTurnoMedico();
                        break;
                    case 5: // Medicos en activo
                        this.medicosEnActivo();
                        break;
                    case 6: // Medicos disponibles
                        this.medicosDisponibles();
                        break;
                    case 7: // Finalizar turno de Medico
                        this.finalizarTurnoMedico();
                        break;
                    case 8: // Nueva incidencia
                        this.nuevaIncidencia();
                        break;
                    case 9: // Finalizar incidencia
                        this.finalizarIncidencia();
                        break;
                    case 10: //Obtener lista en espera
                        this.obtenerIncidenciasEspera();
                        break;
                    case 11: 
                        this.obtenerIncidenciasPaciente();
                        break;
                    case 12:
                        this.obtenerPacientesEspera();
                        break;
                    default: 
                        System.out.println("Opcion incorrecta.");
                        System.out.println();
                        break;
                }

                System.out.print("1. Nuevo Paciente\n"
                           + "2. Actualizar Paciente\n"
                           + "3. Eliminar Paciente\n"
                           + "4. Iniciar Turno Medico\n"
                           + "5. Medicos en activo\n"
                           + "6. Medicos disponibles\n"
                           + "7. Finalizar Turno Medico\n"
                           + "8. Nueva Incidencia\n"
                           + "9. Finalizar Incidencia\n"
                           + "10. Obtener Incidencias en Espera\n"
                           + "11. Obtener Incidencias Paciente\n"
                           + "12. Obtener Pacientes en Espera\n"
                           + "0. Salir\n"
                           + "Introduce una opción: ");

                op = Integer.parseInt(br.readLine());
                System.out.println();
            }
        } catch (Exception e) {
            System.out.println("\n--> " + e.getMessage() + " <--\n");
            this.menu();
        }
        
    }
    
    public void nuevoPaciente() throws Exception{
        try {
            List<Paciente> pacientes = pS.obtenerTodosPaciente();
            
            System.out.print("Introduce Nombre: ");
            String nombre = br.readLine();
            
            System.out.print("Introduce el nº de Seg.Social: ");
            String nSegSoc = br.readLine();
            
           /* for (Paciente pac : pacientes) {
                if (pac.getNombre().equals(nombre) && pac.getnSegSoc().equals(nSegSoc)) {
                    System.out.println("El paciente ya esta registrado");
                    break;
                }else{
                    pS.nuevoPaciente(nombre, nSegSoc);
                }
            }*/
            System.out.println();
            pS.nuevoPaciente(nombre, nSegSoc);
        } catch (Exception e) {
            System.out.println("Error al registrar el nuevo paciente" + e.getLocalizedMessage());
        }
    }
    
    public void modificarPaciente() throws Exception
    {
        Scanner sc;
        System.out.println("Introduce el ID del Paciente a modificar: ");
        int idPaciente = Integer.parseInt(br.readLine());
        
        try
        {
            pS.obtenerPaciente(idPaciente);
            System.out.println("Introduzca el nombre del paciente: ");
            sc= new Scanner(System.in);
            String nombre = sc.nextLine();
            System.out.println("Introduzca el número de la Seguridad Social: ");
            sc= new Scanner(System.in);
            String nSoc = sc.nextLine();
            
            pS.actualizarPaciente(idPaciente, nombre, nSoc);
        }
        catch (Exception e)
        {
            System.out.println("No ha sido posible modificar el paciente." + e.getLocalizedMessage());
        }
    }
    
    public void eliminarPaciente() throws Exception{
        System.out.print("Introduce el ID del Paciente: ");
        int idPaciente = Integer.parseInt(br.readLine());
        
        try {
            pS.eliminarPaciente(idPaciente);
        } catch (Exception e) {
           System.out.println("No se ha podido eliminar el paciente con ID " + idPaciente + " " + e.getLocalizedMessage());
        }
    }
    
    public void iniciarTurnoMedico() throws Exception{
        
        try {
            System.out.print("Introduce el ID del médico para iniciar su turno: ");
            int idMedico = Integer.parseInt(br.readLine());
            
            uS.comenzarTurno(idMedico);
            
        } catch (Exception e) {
//            throw new Exception("No se ha podido iniciar el turno del medico" + e.getLocalizedMessage());
            System.out.println("No se ha podido iniciar el turno del medico" + e.getLocalizedMessage());

        }
    }
    
    public void medicosEnActivo()throws Exception
    {        
        try
        {
            
          List<Medico> lista=mS.getMdao().getMedico();
          if(lista == null)
          {
              throw new Exception("No hay medicos en activo");
          }
            System.out.println();
            System.out.println("<<<< MEDICOS EN ACTIVO ACTUALMENTE >>>>");
            System.out.println("_______________________________________");
            
            for (Medico m : lista)
            {
                if(m.getEstado().equals("OCUPADO"))
                {    
                    System.out.println();
                    
                    System.out.println("ID: " + m.getId());
                    System.out.println("Nombre: " + m.getNombre());
                    System.out.println("Estado: " + m.getEstado());  
                    
                    System.out.println();
                    System.out.println("_______________________________________");
                }
            }
            
//          return lista;
          
        }
        catch(Exception e)
        {
            System.out.println("No se han podido obtener todos los medicos"+e.getLocalizedMessage());
        }
        System.out.println();
    }
    
    public void medicosDisponibles()throws Exception
    {
        List<Medico> lista = mS.obtenerMedicosServicio();
//        List<Medico> disponible=new ArrayList();
        
        try
        {          
            if(lista == null)
            {
               System.out.println("No existen en este momento medicos disponibles.");
            }
            
            System.out.println();
            System.out.println("<<<< MEDICOS DISPONIBLES >>>>");
            System.out.println("_____________________________");
            
            
            for (Medico m : lista)
            {
               
//                    disponible.add(m);
                    System.out.println();
                    
                    System.out.println("ID: " + m.getId());
                    System.out.println("Nombre: " + m.getNombre());
                    System.out.println("Estado: " + m.getEstado());
                    
                    System.out.println();
                    System.out.println("_____________________________");
                
            }
            System.out.println();
        }
        catch (Exception e)
        {
            System.out.println("No se han podido obtener los medicos disponible."+e.getLocalizedMessage());
        }
//        return disponible;
        System.out.println();
    }
    
    public void finalizarTurnoMedico() throws Exception{
        Incidencia i = new Incidencia();
        try {
            System.out.print("Indica el ID del medico que va a finalizar su turno: ");
            int idMedico = Integer.parseInt(br.readLine());
            
            uS.finalizarTurno(idMedico);
            /*Medico m = mdao.getMedico(idMedico);
            
            if (m != null && (i.getMedico() != m) && (!i.getEstado().equals("ACTIVA"))) {
                mS.actualizarMedico(idMedico, m.getNombre(), "FUERA_SERV");
            }else{
                System.out.println("No hay ningun medico con ese ID o tiene incidencias ACTIVAS");
                return;
            }*/
            
        } catch (Exception e) {
            System.out.println("No se ha podido finalizar el turno del medico" + e.getLocalizedMessage());
        }
    }
    
    public void nuevaIncidencia() throws Exception{
       Scanner sc;
       String incidenciaStr;
       int incidencia;
        try {
            System.out.println("\nComente que le ocurre: ");
            String descripcionIncidencia = br.readLine();
            System.out.print("\n1.-Leve\n2.-Grave\n3.-Critica\nTipo de Incidencia: ");
             
                sc= new Scanner(System.in);
                incidenciaStr=sc.nextLine();
                incidencia =Integer.parseInt(incidenciaStr);
            
            
            if(incidencia != 0){
            switch(incidencia){
                case 1:
                    this.nuevaIncidenciaLeve(descripcionIncidencia);
                    break;
                case 2:
                    this.nuevaIncidenciaGrave(descripcionIncidencia);
                    break;
                case 3:
                    this.nuevaIncidenciaCritica(descripcionIncidencia);
                    break;
                default:
                    break;
            
            }
            }
            
        } catch (Exception e) {
            throw new Exception("No se ha podido registrar la nueva incidencia" + e.getLocalizedMessage());
        }
 
    }
    
    public void finalizarIncidencia() throws Exception{
        try {
            System.out.print("Introduce el ID de la incidencia que se quiere finalizar: ");
            int idIncidencia = Integer.parseInt(br.readLine());
            
            uS.finalizarIncidencia(idIncidencia);
            
        } catch (Exception e) {
            System.out.println("No se ha podido finalizar la incidencia"+e.getLocalizedMessage());
        }
        
    }
    
    public void nuevaIncidenciaLeve(String descripcionIncidencia) throws Exception{
        try {
            System.out.println("Introduce tus datos");
        
            System.out.print("Nombre: ");
            String nombrePac = br.readLine();
            
            System.out.print("Numero de Seguridad Social: ");
            String nSegSoc = br.readLine();
          
            System.out.println();
            List<Paciente> pacientes = pS.obtenerTodosPaciente();
            
            for (Paciente pac : pacientes) {
                if (pac.getNombre().equals(nombrePac) && pac.getnSegSoc().equals(nSegSoc)) {
                    System.out.println("El paciente ya existe y se le asigna incidencia");
                     uS.registrarIncidenciaLeve(pac.getIdPaciente(), descripcionIncidencia);
                    return;
                }
            }
                    pS.nuevoPaciente(nombrePac, nSegSoc);
                    Paciente p=pS.getIdao().getPacienteSeguridad(nSegSoc);
                    uS.registrarIncidenciaLeve(p.getIdPaciente(), descripcionIncidencia);
                   
            
        } catch (Exception e) {
            throw new Exception("Error al intentar registrar la nueva incidencia Leve" + e.getLocalizedMessage());
        }
        
    }
    public void nuevaIncidenciaGrave(String descripcionIncidencia) throws Exception{
        try {
            System.out.println("Introduce tus datos");
        
            System.out.print("Nombre: ");
            String nombrePac = br.readLine();
            
            System.out.print("Numero de Seguridad Social: ");
            String nSegSoc = br.readLine();
            
            List<Paciente> pacientes = pS.obtenerTodosPaciente();
            
            for (Paciente pac : pacientes) {
                if (pac.getNombre().equals(nombrePac) && pac.getnSegSoc().equals(nSegSoc)) {
                    System.out.println("El paciente ya existe y se le asigna la incidencia");
                    break;
                }
                   
             }
                    pS.nuevoPaciente(nombrePac, nSegSoc);
                       Paciente p=pS.getIdao().getPacienteSeguridad(nSegSoc);
                    uS.registrarIncidenciaGrave(p.getIdPaciente(), descripcionIncidencia);
        } catch (Exception e) {
           throw new Exception("Error al intentar registrar la nueva incidencia Leve"+e.getLocalizedMessage());
        }
    }
    public void nuevaIncidenciaCritica(String descripcionIncidenciaCritica) throws Exception{
        
        try {
            System.out.print("Introduce el ID del paciente: ");
            int idPaciente = Integer.parseInt(br.readLine());
            
            Paciente p = pS.obtenerPaciente(idPaciente);

            if (p != null) {
                uS.registrarIncidenciaCritica(p.getIdPaciente(), descripcionIncidenciaCritica);
            }else{
                p.setIdPaciente(-1);
                pS.getIdao().addPaciente(p);
                uS.registrarIncidenciaCritica(p.getIdPaciente(), descripcionIncidenciaCritica);
            }
            
        } catch (Exception e) {
            throw new Exception("Error al intentar registrar la nueva incidencia Critica"+e.getLocalizedMessage());
        }  
    }
    
    public void obtenerIncidenciasEspera() throws Exception{
        List<Incidencia> listaEspera;
        try{
             listaEspera= uS.obtenerEspera();
            if(!listaEspera.isEmpty()){
                
                 
                  System.out.println();
                  System.out.println("<<<< INCIDENCIAS EN ESPERA >>>>");
                  System.out.println("_______________________________");
                  
                //recorro la lista de incidencias
                 for(Incidencia i : listaEspera){
                     if(i.getEstado().equals("ESPERA")){
                         System.out.println();
                         
                         System.out.println("Id: "+i.getIdIncidencia());
                         System.out.println("Descripción: "+i.getDescripcion());
                         System.out.println("Estado: "+i.getEstado());
                         System.out.println("Fecha de Entrada: "+i.getFechaEntrada());
                         System.out.println("Número de Espera: "+i.getNumEspera());
                         System.out.println("Tipo: "+i.getTipo());
                         System.out.print("Paciente:\n" +
                                          "\t-> Nombre: " + i.getPaciente().getNombre() + "\n" + 
                                          "\t-> Numero de Seguridad Social: " + i.getPaciente().getnSegSoc());
                         
                         System.out.println();
                         System.out.println("_______________________________");
                     }
                     
                     System.out.println();
                }
                 
            }else{
               System.out.println("No hay incidencias en espera");
            }
             
            
        }catch(Exception e){
            System.out.println("Error al mostrar cola de Incidencias en Espera"+e.getLocalizedMessage());
        }
        System.out.println();
    }
        public void obtenerIncidenciasPaciente() throws Exception{
            int cont = 0;
            Paciente p;
            List<Incidencia> incidencias;
            
            
            try{
                System.out.println("Introduzca el numero de la Seguridad Social: ");
                String numSeg=br.readLine();
                p=pS.getIdao().getPacienteSeguridad(numSeg);
                int id=p.getIdPaciente();
                incidencias=pS.obtenerIncidenciasPaciente(id);
                 
                 System.out.println();
                 System.out.println("<<<< INCIDENCIAS DEL PACIENTE "+p.getNombre()+" >>>>");
                 System.out.println("____________________________________________________");
                 
                for(Incidencia i : incidencias){
                    
                         System.out.println();
                     
                         System.out.println("Id: "+i.getIdIncidencia());
                         System.out.println("Descripción: "+i.getDescripcion());
                         System.out.println("Estado: "+i.getEstado());
                         System.out.println("Fecha de Entrada: "+i.getFechaEntrada());
                         System.out.println("Número de Espera: "+i.getNumEspera());
                         System.out.println("Tipo: "+i.getTipo());
                         //1System.out.println("Paciente:"+i.getPaciente());
                         
                         System.out.println("___________________________________________________");
                     
                 }
                System.out.println();
                
            }catch(Exception e){
               System.out.println("Error al obtener la incidenias del Paciente"+e.getLocalizedMessage());
            }
            System.out.println();
        }
        
        public void obtenerPacientesEspera() throws Exception{
            List<Paciente> pacientes;
            try{
                pacientes=pS.obtenerPacientesEnEspera();
                
                if(pacientes!=null){
                    
                     System.out.println();
                     System.out.println("<<<< PACIENTES EN ESPERA >>>>");
                     System.out.println("_____________________________");
                     
                    for(Paciente p:pacientes){
                        
                        System.out.println();
                        
                        System.out.println("Id: "+p.getIdPaciente());
                        System.out.println("Nombre: "+p.getNombre());
                        System.out.println("Num. Seguridad Social "+p.getnSegSoc()+"\n");
                        
                        System.out.println("______________________________");
                        
                    }
                    System.out.println();
                    
                }else{
                    System.out.println("No hay pacientes en espera");
                }
                
                
                
            }catch(Exception e){
                System.out.println("Error al obtener Pacientes en Espera"+e.getLocalizedMessage());
            }
            System.out.println();
        }
     
 }

