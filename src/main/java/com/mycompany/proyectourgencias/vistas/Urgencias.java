package com.mycompany.proyectourgencias.vistas;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Urgencias {
    public static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    
    public static void main(String[] args) throws Exception{
        Login lg = new Login();
        lg.iniciarSesion();
        System.out.print("Elige el menu al que quieres acceder:\n" + 
                         "1. Menu Administrativo\n" + 
                         "2. Menu Directivo\n" + 
                         "3. Menu Sanitario\n\n" + 
                         "Introduce una opción: ");
        try {
            int op = Integer.parseInt(br.readLine());
            
            while (op != 0) {                
                switch(op){
                case 1: // PrincipalAdministrativo
                    PrincipalAdministrativo pA = new PrincipalAdministrativo();
                    pA.menu();
                    break;
                case 2: // PrincipalDirectivo
                    PrincipalDirectivo pD = new PrincipalDirectivo();
                    pD.menu(); 
                    break;
                case 3: // PrincipalSanitario
                     PrincipalSanitario pS=new PrincipalSanitario();
                     pS.menu();
                    break;
                default:
                    System.out.println("Opción incorrecta");
                    break;
                }
            }
        } catch (Exception e) {
            throw new Exception("Ha ocurrido un problema al intentar leer desde teclado",e);
        }
        
        

    }
}
