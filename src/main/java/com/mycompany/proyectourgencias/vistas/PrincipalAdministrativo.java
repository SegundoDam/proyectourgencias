package com.mycompany.proyectourgencias.vistas;

import com.mycompany.proyectourgencias.entidades.Medico;
import com.mycompany.proyectourgencias.entidades.Usuario;
import com.mycompany.proyectourgencias.servicio.MedicoService;
import com.mycompany.proyectourgencias.servicio.UsuarioService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class PrincipalAdministrativo {
    private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    private UsuarioService uS = new UsuarioService();
    private MedicoService mS = new MedicoService();
    
    public PrincipalAdministrativo(){
        System.out.println("---------------------------------");
        System.out.println("--MENU-PRINCIPAL-ADMINISTRATIVO--");
        System.out.println("---------------------------------");
    }
    public void menu() throws Exception{
        System.out.print("1. Nuevo Usuario\n" + 
                                 "2. Nuevo Medico\n" +
                                 "3. Borrar Usuario\n" +
                                 "4. Borrar Medico\n" +
                                 "5. Actualizar Usuario\n" +
                                 "6. Actualizar Medico\n" +
                                 "7. Listar todos los Usuarios\n" +
                                 "8. Listar todos los Medicos\n" +
                                 "9. Listar los Medicos Activos\n" + 
                                 "0. Salir\n\n" +
                                 "Introduce una opcion: ");
        
        try {
            int op = Integer.parseInt(br.readLine());
           
            while (op != 0) {                
                switch(op){
                    case 1: // Nuevo Usuario
                        this.nuevoUsuario();
                        break;
                    case 2: // Nuevo Medico
                        this.nuevoMedico();
                        break;
                    
                    case 3: // Eliminar Usuario
                        this.eliminarUsuario();
                        break;
                        
                    case 4: // Eliminar Medico
                        this.eliminarMedico();
                        break;
                  
                    case 5: // Actualizar Usuario
                        this.actualizarUsuario();
                        break;
                        
                    case 6: // Actualizar Medico
                        this.actualizarMedico();
                        break;
                    
                    case 7: // Listar todos los usuarios
                        this.obtenerTodosUsuarios();
                        break;
                    
                    case 8: // Listar todos los Medicos
                        this.obtenerTodosLosMedicos();
                        break;
                    
                    case 9: // Listar los Medicos Activos
                        this.obtenerMedicosActivos();
                        break;
                    
                    default: 
                        System.out.println("Opcion incorrecta");
                        break;
                }
                System.out.print("1. Nuevo Usuario\n" + 
                                 "2. Nuevo Medico\n" +
                                 "3. Borrar Usuario\n" +
                                 "4. Borrar Medico\n" +
                                 "5. Actualizar Usuario\n" +
                                 "6. Actualizar Medico\n" +
                                 "7. Listar todos los Usuarios\n" +
                                 "8. Listar todos los Medicos\n" +
                                 "9. Listar los Medicos Activos\n" + 
                                 "0. Salir\n\n" +
                                 "Introduce una opcion: ");
                op = Integer.parseInt(br.readLine());
            }
        } catch (Exception e) {
            System.out.println("\n--> "+ e.getMessage() + " <--\n");
            this.menu();
        }
    }
    
    public void nuevoUsuario() throws Exception{
        try {
            System.out.print("Nombre: ");
            String nombre = br.readLine();
            while(nombre.equals(""))
            {
                System.out.println("no has introducido ningun valor");
                System.out.print("Nombre: ");
                nombre = br.readLine();
            }

            System.out.print("Usuario: ");
            String usuario = br.readLine();
            while(usuario.equals(""))
            {
                System.out.println("no has introducido ningun valor");
                System.out.print("Usuario: ");
                usuario = br.readLine();
            }

            System.out.print("Password: ");
            String pwd = br.readLine();
            while(pwd.equals(""))
            {
                System.out.println("no has introducido ningun valor");
                System.out.print("Password: ");
                pwd = br.readLine();
            }
            
            System.out.print("¿A que rol pertenece? (Administrativo, Sanitario, Directivo): ");
            String rol = br.readLine();
            while(rol.equals("Administrativo")!=true && rol.equals("Sanitario")!=true && rol.equals("Directivo")!=true)
            {
                System.out.println("El rol introducido no es valido");
                System.out.print("¿A que rol pertenece? (Administrativo, Sanitario, Directivo): ");
                rol = br.readLine();
            }
            
            uS.nuevoUsuario(nombre, usuario, pwd, rol);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    
    public void eliminarUsuario() throws Exception{
        System.out.print("Introduce el id del usuario que quieres eliminar: ");
        
        try {
            boolean encontrado =false;
            boolean salida =false;
            int id = Integer.parseInt(br.readLine());
            List<Usuario> usuarios = uS.obtenerUsuarios();
            for (Usuario usu : usuarios) 
            {
                if(usu.getId()==id)
                {
                    encontrado =true;
                } 
            }
            while(encontrado == false)
            {
                System.out.println("No existe ningun usuario con ese id");
                System.out.print("Introduce el id del usuario que quieres eliminar si no quieres eliminar ningun usuario introduce 0: ");
                id = Integer.parseInt(br.readLine());
                if(id==0)
                {
                    salida =true;
                    break;
                }
                usuarios = uS.obtenerUsuarios();
                for (Usuario usu : usuarios) 
                {
                    if(usu.getId()==id)
                    {
                        encontrado =true;
                    } 
                }
            }
            if(salida!=true)
                {
                    uS.eliminarUsuario(id);
                }   
        } catch (Exception e) {
            throw new Exception("Error al intentar borrar el usuario seleccionado",e);
        }
    }
    
    public void actualizarUsuario() throws Exception{
      try{
            Scanner sc;
            
            System.out.println("Introduce la id del usuario a modificar");
            sc = new Scanner(System.in);
            int id = sc.nextInt();
            System.out.println("Introduzca el nuevo nombre personal");
            sc = new Scanner(System.in);
            String nombre = sc.nextLine();
            System.out.println("Introduzca el nombre del nuevo usuario");
            sc = new Scanner(System.in);
            String usu = sc.nextLine();
            System.out.println("Introduzca la nueva contraseña del usuario");
            sc = new Scanner(System.in);
            String pwd = sc.nextLine();
            System.out.println("Introduzca el rol que tiene el usuario");
            sc = new Scanner(System.in);
            String rol = sc.nextLine();
            uS.actualizarUsuario(id, nombre, usu, pwd, rol);
        }
        catch(Exception e){
         System.out.println("Error al actualizar el usuario");   
        }
    }

    
        /* try {
            boolean encontrado =false;
            boolean salida =false;
            String nombre;
            String usuario;
            String pwd;
            String rol;
            System.out.print("ID a actualizar: ");
            int id = Integer.parseInt(br.readLine());
            List<Usuario> usuarios = uS.obtenerUsuarios();
            for (Usuario usu : usuarios) 
            {
                if(usu.getId()==id)
                {
                    encontrado =true;
                } 
            }
            while(encontrado == false)
            {
                System.out.println("No existe ningun usuario con ese id");
                System.out.print("Introduce el id del usuario que quieres eliminar si no quieres eliminar ningun usuario introduce 0: ");
                id = Integer.parseInt(br.readLine());
                if(id==0)
                {
                    salida =true;
                    break;
                }
                usuarios = uS.obtenerUsuarios();
                for (Usuario usu : usuarios) 
                {
                    if(usu.getId()==id)
                    {
                        encontrado =true;
                    } 
                }
            }
            if(salida!=true)
                {
                    
                
                System.out.print("Nombre: ");
                nombre = br.readLine();
                while(nombre.equals(""))
                {
                    System.out.println("no has introducido ningun valor");
                    System.out.print("Nombre: ");
                    nombre = br.readLine();
                }

                System.out.print("Usuario: ");
                usuario = br.readLine();
                while(usuario.equals(""))
                {
                    System.out.println("no has introducido ningun valor");
                    System.out.print("Usuario: ");
                    usuario = br.readLine();
                }

                System.out.print("Password: ");
                pwd = br.readLine();
                while(pwd.equals(""))
                {
                    System.out.println("no has introducido ningun valor");
                    System.out.print("Password: ");
                    pwd = br.readLine();
                }

                System.out.print("Â¿A quÃ© rol va ha pertenecer? (Administrativo, Sanitario): ");
                rol = br.readLine();
                while(rol.equals("Administrativo")!=true && rol.equals("Sanitario")!=true && rol.equals("Directivo")!=true)
                {
                    System.out.println("El rol introducido no es valido");
                    System.out.print("¿A que rol pertenece? (Administrativo, Sanitario, Directivo): ");
                    rol = br.readLine();
                }
                
                uS.actualizarUsuario(id, nombre, usuario, pwd, rol);
             }

            
        } catch (IOException e) {
            throw new Exception("Error. No se ha podido leer desde teclado",e);
        }*/
    
    
    public void obtenerTodosUsuarios()throws Exception{
        try
            {
               List<Usuario> usuarios = uS.obtenerUsuarios();

                 System.out.println();
                 System.out.println("<<<< LISTADO DE TODOS LOS USUARIOS >>>>");
                 System.out.println("_______________________________________");
                 System.out.println();
                 
                for (Usuario usu : usuarios) 
                {
                    System.out.println("ID: " + usu.getId());
                    System.out.println("Nombre: " + usu.getNombre());
                    System.out.println("Contraseña: " + usu.getPwd()); 
                    
                    System.out.println("_____________________________________");
                    System.out.println();
                }
            }
        catch(Exception e)
        {
            throw new Exception("Ha habido un error al obtener la lista de usuarios",e);
        }
        System.out.println();
    }
    
    public void nuevoMedico() throws Exception{
        try {
            System.out.print("Nombre del medico: ");
            String nombreMedico = br.readLine();
            
            mS.nuevoMedico(nombreMedico);
        } catch (Exception e) {
            throw new Exception("No se ha podido registrar el nuevo medico ", e);
        }
    }
    
    public void eliminarMedico() throws Exception{
        try {
            System.out.printf("Nombre del medico a borrar: ");
            String nombreMedico = br.readLine();
            
            List<Medico> listaMedicos = mS.obtenerTodosMedicos();
            
            for (Medico med : listaMedicos) {
                if (med.getEstado().equals("OCUPADO")) {
                    System.out.println("El medico esta de servicio. No puede ser borrado");
                    break;
                }else{
                    if (med.getNombre().equals(nombreMedico)) {
                        mS.eliminarMedico(med.getId());
                        break;
                    }
                }
            }
        } catch (Exception e) {
            throw new Exception("No se ha podido borrar el medico indicado"+ e.getLocalizedMessage());
        }
    }
    
    public void actualizarMedico() throws Exception{
        try {
            System.out.print("Introduce el Numero Identificador del medico a actualizar: ");
            int id = Integer.parseInt(br.readLine());
            
            System.out.print("Introduce el nuevo nombre para el medico con el ID " + id + " : ");
            String nombre = br.readLine();
            
            System.out.print("Indica en que estado se encuentra el Medico (Disponible, Ocupado, Fuera_Serv): ");
            String estadoMedico = br.readLine();
            
            mS.actualizarMedico(id, nombre, estadoMedico);
        } catch (Exception e) {
            throw new Exception("Se ha producido una excepcion de tipo: " + e.getLocalizedMessage() + " al intentar actualizar el Medico");
        }
        
    }
    
    public void obtenerTodosLosMedicos() throws Exception{
        try {
            List<Medico> listaMedicos = mS.obtenerTodosMedicos();
            
            if (!listaMedicos.isEmpty()) {
                
                System.out.println();
                System.out.println("<<<< LISTADO DE TODOS LOS MEDICOS >>>>");
                System.out.println("______________________________________");
                System.out.println();
                
                for (Medico med : listaMedicos) {
                    System.out.print("ID Medico: " + med.getId() + "\n" + 
                                    "Nombre del Medico: " + med.getNombre() + "\n" + 
                                    "Estado del Medico: " + med.getEstado() + "\n\n");
                    
                    System.out.println("_____________________________________");
                }
            }else{
                System.out.println("\n No hay medicos registrados en este momento \n");
            }
        } catch (Exception e) {
            throw new Exception("Se ha producido una excepcion de tipo : " + e.getLocalizedMessage() + " al intentar obtener todos los Medicos");
        }
        System.out.println();
    }
    
    public void obtenerMedicosActivos() throws Exception{
        try {
            List<Medico> listaMedicosActivos = mS.obtenerMedicosServicio();
            
            if (!listaMedicosActivos.isEmpty()) {
                
                System.out.println();
                System.out.println("<<<<LISTADO DE MEDICOS ACTIVOS >>>>");
                System.out.println("___________________________________");
                System.out.println();
                
                for (Medico med : listaMedicosActivos) {
                    System.out.print("ID Medico: " + med.getId() + "\n" + 
                                     "Nombre del Medico: " + med.getNombre() + "\n");
                    
                    System.out.println("__________________________________");
                }
            }else{
                System.out.println("\n No hay medicos activos en este momento \n");
            }
        } catch (Exception e) {
            throw new Exception("Se ha producido una excepcion de tipo : " + e.getLocalizedMessage() + " al intentar obtener los Medicos Activos");
        }
        System.out.println();
    }
}
