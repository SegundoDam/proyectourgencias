/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.proyectourgencias.vistas;

import com.mycompany.proyectourgencias.datos.IncidenciaDAO;
import com.mycompany.proyectourgencias.entidades.Incidencia;
import com.mycompany.proyectourgencias.entidades.Medico;
import com.mycompany.proyectourgencias.entidades.Paciente;
import com.mycompany.proyectourgencias.servicio.MedicoService;
import com.mycompany.proyectourgencias.servicio.PacienteService;
import com.mycompany.proyectourgencias.servicio.UrgenciasService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author Marco Antonio
 */
public class PrincipalDirectivo {

    UrgenciasService Us = new UrgenciasService();
    IncidenciaDAO Id = new IncidenciaDAO(); 
    PacienteService Ps = new PacienteService();
    MedicoService Ms = new MedicoService();
    public static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        public PrincipalDirectivo(){

        System.out.println("---------------------------------");
        System.out.println("--MENU-PRINCIPAL-DIRECTIVO--");
        System.out.println("---------------------------------");
    }
        
        public void menu() throws Exception
        {
            
            int opcion=-1;
            try{
                while(opcion!=0)
                {
                System.out.println();
                System.out.print("1.Todas las incidencias\n" + 
                         "2.Incidencias Historicas\n" +
                         "3.Todos los pacientes\n" +
                         "4.Pacientes atendidos en este momento\n" +
                         "5.Incidencias criticas atendidas en este momento\n" +
                         "6.Incidencias criticas historicas\n" +
                         "7.Tiempo medio de atencion en incidencias criticas\n" +
                         "8.Tiempo medio de atencion en incidencias leves\n" +
                         "9.Tiempo medio de atencion en incidencias Graves\n" +
                         "10.Numero de pacientes que estan en la cola de incidencias graves\n" +
                         "11.Medico que mas incidencias ha resuelto\n" +
                         "12.Medico que mas veces ha atendido una atencion critica\n" +
                         "13.Listado de incidencias resueltas por un medico\n" +
                         "0. Salir\n\n" +
                         "Introduce una opcion: ");
                
                Scanner sc = new Scanner(System.in);
                opcion = sc.nextInt();
                if(opcion==1)
                {
                    this.getIncidencias();
                    
                }
                if(opcion==2)
                {
                    this.getHistoricIncidences();
                    
                }
                if(opcion==3)
                {
                    this.getPacientes();
                   
                }
                if(opcion==4)
                {
                    this.getAtentionPacients();
                    
                }
                if(opcion==5)
                {
                    this.getCriticsIncidencesAtentioning();
                    
                }
                if(opcion==6)
                {
                    this.getHistoricCriticIncidence();
                    
                }
                if(opcion==7)
                {
                    this.getTimeCritics();
                                        
                }
                if(opcion==8)
                {
                    this.getTimeLeves();
                    
                }
                if(opcion==9)
                {
                    this.getTimeGraves();
                    
                }
                if(opcion==10)
                {
                    this.getCountPatientsinCola();
                    
                }
                if(opcion==11)
                {
                    this.getMedMaxInc();
                    
                }
                if(opcion==12)
                {
                    this.getMedMaxCritic();
                    
                }
                if(opcion==13)
                {
                    this.obtenerIncidenciasMedico();
                    
                }
                if(opcion==0)
                {
                    System.out.println("Principal directivo cerrado");
                    break;
                }
                
                }
            }
             catch(InputMismatchException ex2){
                     System.out.println("Debe introducir un valor numerico");
                     System.out.println("");
                     this.menu();

                 }catch (Exception e) {
                         System.out.println("\n--> " + e.getMessage() + " <--\n");
                         this.menu();
                 }
        }
        public void getIncidencias()
        {
            /*
            try
            {
                List<Incidencia> incidencias = Id.getIncidencias();
                System.out.println("-----------");
                System.out.println("Incidencias");
                System.out.println("-----------");
                for(Incidencia i: incidencias)
                {
                    System.out.println(""+i.getIdIncidencia()+ " "+ i.getDescripcion()+" " + i.getFechaEntrada()+" "+i.getFechaAtencion()+ " "+i.getFechaSalida()+" "+i.getEstado()+" "+i.getMedico()+" "+i.getPaciente()+" "+i.getTipo()+" "+i.getNumEspera());
                    System.out.println();
                }
            }
            catch (NullPointerException ex2) {
            System.out.println("Error: No se han obtenido todas las incidencias correctamente");
            System.out.println("");
            }
            catch (Exception ex) {
            System.out.println("Error:"+ ex.getLocalizedMessage());
            System.out.println("");
            
        }*/
           try
            {
              // List<Incidencia> incidencias = Us.obtenerIncidencias();
               List<Incidencia> incidencias =Id.getIncidencias();
               Medico med = null;
               Paciente pac = null;
                 
                 System.out.println();
                 System.out.println("<<<< LISTADO DE INCIDENCIAS >>>>");
                 System.out.println("________________________________");
                 System.out.println();
                 
                for (Incidencia inc : incidencias) 
                {
                                        
                    System.out.println("ID: " + inc.getIdIncidencia());
                    System.out.println("Descripcion: " + inc.getDescripcion());
                    System.out.println("Tipo: " + inc.getTipo());
                    System.out.println("Estado: " + inc.getEstado());
                    System.out.println("Fecha de Inicio: " + inc.getFechaEntrada());
                    if(inc.getEstado().equals("ESPERA")== false)
                    {
                        System.out.println("Fecha de Atencion: " + inc.getFechaAtencion());
                        if(inc.getEstado().equals("HISTORICA")== true)
                        {
                            System.out.println("Fecha de Finalizacion: " + inc.getFechaSalida());
                        }
                        med = inc.getMedico();
                        System.out.println("Medico Asignado: " + med.getNombre());
                        
                        System.out.println("");
                        System.out.println("_______________________________");
                    }
                    else
                    {
                        System.out.println("Numero de Espera: " + inc.getNumEspera());
                    }

                }
            }
        catch(Exception e)
        {
            //throw new Exception("Ha habido un error al obtener la lista de incidencias"+e.getLocalizedMessage());
            System.out.println("Error:"+ e.getLocalizedMessage());
        }
           System.out.println();
            
       }
        public void getHistoricIncidences()
        {
            /*
            try
            {
                List<Incidencia> incidencias = Us.obtenerHistoricas();
                System.out.println("----------------------");
                System.out.println("Incidencias Históricas");
                System.out.println("----------------------");
                for(Incidencia i: incidencias)
                {
                    System.out.println(""+i.getIdIncidencia()+ " "+ i.getDescripcion()+" " + i.getFechaEntrada()+" "+i.getFechaAtencion()+ " "+i.getFechaSalida()+" "+i.getEstado()+" "+i.getMedico()+" "+i.getPaciente()+" "+i.getTipo()+" "+i.getNumEspera());
                    System.out.println();
                }
            }
            catch (NullPointerException ex2) {
            System.out.println("Error: No se han obtenido los incidencias históricas correctamente");
            System.out.println("");
            }
            catch (Exception ex) {
            System.out.println("Error:"+ ex.getLocalizedMessage());
            System.out.println("");
            
        }*/
            try
            {
               List<Incidencia> incidencias = Us.obtenerHistoricas();
                 
                 System.out.println();
                 System.out.println("<<<< HISTORICO DE INCIDENCIAS >>>>");
                 System.out.println("__________________________________");
                 System.out.println();
                 
                for (Incidencia inc : incidencias) 
                {   Medico med = null;
                    Paciente pac = null;
                    System.out.println("ID: " + inc.getIdIncidencia());
                    System.out.println("Descripcion: " + inc.getDescripcion());
                    System.out.println("Tipo: " + inc.getTipo());
                    //System.out.println("Estado: " + inc.getEstado());
                    //Comento este metodo por que solo mostraria Historica en todas
                    System.out.println("Fecha de Inicio: " + inc.getFechaEntrada());
                    System.out.println("Fecha de Atencion: " + inc.getFechaAtencion());
                    System.out.println("Fecha de Finalizacion: " + inc.getFechaSalida());
                    med = inc.getMedico();
                    System.out.println("Medico Asignado: " + med.getNombre());
                    pac = inc.getPaciente();
                    System.out.println("Paciente: " + pac.getNombre());
                    
                    System.out.println("");
                    System.out.println("__________________________________");
                }
            }
        catch(Exception e)
        {
             System.out.println("Ha habido un error al obtener la lista de incidencias"+e.getLocalizedMessage());
            //throw new Exception("Ha habido un error al obtener la lista de incidencias",e);
        }
            System.out.println();
        }
        public void getPacientes()
        {
            /*
            try
            {
                List<Paciente> pacientes = Ps.obtenerTodosPaciente();
                System.out.println("----------------------");
                System.out.println("Pacientes");
                System.out.println("----------------------");
                for(Paciente p: pacientes)
                {
                    System.out.println(""+p.getIdPaciente()+" "+ p.getNombre()+" "+ p.getnSegSoc()+" ");
                    System.out.println();
                }
            }
            catch (NullPointerException ex2) {
            System.out.println("Error: No se han obtenido los pacientes correctamente");
            System.out.println("");
            }
            catch (Exception ex) {
            System.out.println("Error:"+ ex.getLocalizedMessage());
            System.out.println("");
            
        }*/
            try
            {
               List<Paciente> pacientes = Ps.obtenerTodosPaciente();
               Set<Incidencia> incidencias = null;
                 
                 System.out.println();
                 System.out.println("<<<< LISTADO DE PACIENTES >>>>");
                 System.out.println("______________________________");
                 System.out.println();
                 
                for (Paciente pac : pacientes) 
                {
                    System.out.println("ID: " + pac.getIdPaciente());
                    System.out.println("Nombre: " + pac.getNombre());
                    System.out.println("Numero Seguridad Social: " + pac.getnSegSoc());
                    incidencias = pac.getIncidencias();
                    for(Incidencia inc : incidencias)
                    {
                       System.out.println("ID incidencia: " + inc.getIdIncidencia());
                       System.out.println("Descripcion: " + inc.getDescripcion());
                       System.out.println("Tipo Incidencia: " + inc.getTipo());
                       System.out.println("Estado Incidencia: " + inc.getEstado());
                       if(inc.getEstado().equals("ESPERA")==false)
                       {
                           Medico med = inc.getMedico();
                           System.out.println("Medico Asignado: " + med.getNombre());
                       }
                       else
                       {
                           System.out.println("Posicion Cola: " + inc.getNumEspera());
                       }
                    }
                    
                    System.out.println();
                    System.out.println("_____________________________");
                }
            }
        catch(Exception e)
        {
            //throw new Exception("Ha habido un error al obtener la lista de pacientes",e);
            System.out.println("Ha habido un error al obtener la lista de pacientes"+e.getLocalizedMessage());
        }
            System.out.println();
        }
        public void getAtentionPacients()
        {/*
            try
            {
                List<Paciente> pacientes = Ps.obtenerPacientesEnAtencion();
                System.out.println("----------------------");
                System.out.println("Pacientes en Atención");
                System.out.println("----------------------");
                for(Paciente p: pacientes)
                {
                    System.out.println(""+p.getIdPaciente()+" "+ p.getNombre()+" "+ p.getnSegSoc()+" ");
                    System.out.println();
                }
            }
            catch (NullPointerException ex2) {
            System.out.println("Error: No se han obtenido los pacientes en atención correctamente");
            System.out.println("");
            }
            catch (Exception ex) {
            System.out.println("Error:"+ ex.getLocalizedMessage());
            System.out.println("");
            
        }*/
            try
        {
               List<Paciente> pacientes = Ps.obtenerPacientesEnAtencion();
               Set<Incidencia> incidencias = null;

                 System.out.println();
                 System.out.println("<<<< LISTADO DE PACIENTES ATENDIDOS >>>>");
                 System.out.println("________________________________________");
                 System.out.println();
                 
                for (Paciente pac : pacientes) 
                {
                    System.out.println("ID: " + pac.getIdPaciente());
                    System.out.println("Nombre: " + pac.getNombre());
                    System.out.println("Numero Seguridad Social: " + pac.getnSegSoc());
                    
                    incidencias = pac.getIncidencias();
                    /* NO PERMITE VER LAS INCIDENCIAS DE PACIENTE VER RAZON
                    for(Incidencia inc : incidencias)
                    {
                        if(inc.getEstado().equals("ACTIVA")==true)
                        {
                           
                            System.out.println("ID incidencia: " + inc.getIdIncidencia());
                            System.out.println("Descripcion: " + inc.getDescripcion());
                            System.out.println("Tipo Incidencia: " + inc.getTipo());
                            //System.out.println("Estado Incidencia: " + inc.getEstado());
                            //Se supone que el estado es activo a si que no lo mostramos
                            Medico med = inc.getMedico();
                            System.out.println("Medico Asignado: " + med.getNombre());
                        }
                    }
                            */
                    
                    System.out.println();
                    System.out.println("_______________________________________");
                }
        }
        catch(Exception e)
        {
            //throw new Exception("Ha habido un error al obtener la lista de pacientes",e);
            System.out.println("Ha habido un error al obtener la lista de pacientes"+e.getLocalizedMessage());
        }
            System.out.println();
        }
        public void getCriticsIncidencesAtentioning()
        {
            /*
             try
            {
                List<Incidencia> incidencias = Us.obtenercriticasatendidas();
                System.out.println("-----------");
                System.out.println("Incidencias en críticas en atención");
                System.out.println("-----------");
                for(Incidencia i: incidencias)
                {
                    System.out.println(""+i.getIdIncidencia()+ " "+ i.getDescripcion()+" " + i.getFechaEntrada()+" "+i.getFechaAtencion()+ " "+i.getFechaSalida()+" "+i.getEstado()+" "+i.getMedico()+" "+i.getPaciente()+" "+i.getTipo()+" "+i.getNumEspera());
                    System.out.println();
                }
            }
            catch (NullPointerException ex2) {
            System.out.println("Error: No se han obtenido todas las incidencias que se están atendiendo y son críticas correctamente");
            System.out.println("");
            }
            catch (Exception ex) {
            System.out.println("Error:"+ ex.getLocalizedMessage());
            System.out.println("");
            
        }*/
            try
        {
            List<Incidencia> incidencias = null; 
            incidencias = Us.obtenercriticasatendidas();
            
            System.out.println();
            System.out.println("<<<< LISTADO DE INCIDENCIAS CRTITICAS ACTIVAS >>>>");
            System.out.println("__________________________________________________");
            System.out.println();
            
            for (Incidencia inc : incidencias)
            {
                Medico med = null;
                Paciente pac = null;
                System.out.println("ID: " + inc.getIdIncidencia());
                System.out.println("Descripcion: " + inc.getDescripcion());
                System.out.println("Fecha de Inicio: " + inc.getFechaEntrada());
                System.out.println("Fecha de Atencion: " + inc.getFechaAtencion());
                med = inc.getMedico();
                System.out.println("Medico Asignado: " + med.getNombre());
                pac = inc.getPaciente();
                System.out.println("Paciente: " + pac.getNombre());
                
                System.out.println("");
                System.out.println("_________________________________________________");
            }
        }
       catch(Exception e) 
        {
            System.out.println("Error al obtener la lista de incidencias criticas activas." +e.getLocalizedMessage()); 
            //throw new Exception("Error al obtener la lista de incidencias criticas activas."); 
        }
            System.out.println();
        }
        public void getHistoricCriticIncidence()
        {
            /*
             try
            {
                List<Incidencia> incidencias = Us.obtenercriticashistoricas();
                System.out.println("-----------");
                System.out.println("Incidencias en críticas históricas");
                System.out.println("-----------");
                for(Incidencia i: incidencias)
                {
                    System.out.println(""+i.getIdIncidencia()+ " "+ i.getDescripcion()+" " + i.getFechaEntrada()+" "+i.getFechaAtencion()+ " "+i.getFechaSalida()+" "+i.getEstado()+" "+i.getMedico()+" "+i.getPaciente()+" "+i.getTipo()+" "+i.getNumEspera());
                    System.out.println();
                }
            }
            catch (NullPointerException ex2) {
            System.out.println("Error: No se han obtenido todas las incidencias históricas y son críticas correctamente.");
            System.out.println("");
            }
            catch (Exception ex) {
            System.out.println("Error:"+ ex.getLocalizedMessage());
            System.out.println("");
        }
                    */
            
       
        try
        {
            List<Incidencia> incidencias = null; 
            incidencias = Us.obtenercriticashistoricas();
            
            System.out.println();
            System.out.println("<<<< HISTORICO DE INCIDENCIAS CRITICAS >>>>");
            System.out.println("___________________________________________");
            System.out.println();
            
            for (Incidencia inc : incidencias)
            {
                Medico med = null;
                Paciente pac = null;
                System.out.println("ID: " + inc.getIdIncidencia());
                System.out.println("Descripcion: " + inc.getDescripcion());
                System.out.println("Fecha de Inicio: " + inc.getFechaEntrada());
                System.out.println("Fecha de Atencion: " + inc.getFechaAtencion());
                System.out.println("Fecha de Finalizacion: " + inc.getFechaSalida());
                med = inc.getMedico();
                System.out.println("Medico Asignado: " + med.getNombre());
                pac = inc.getPaciente();
                System.out.println("Paciente: " + pac.getNombre());
                
                System.out.println();
                System.out.println("__________________________________________");
            }
        }
       catch(Exception e) 
        {
            System.out.println("Error al obtener incidencias criticas historicas."+e.getLocalizedMessage()); 
            //throw new Exception("Error al obtener incidencias criticas historicas."); 
        }
        System.out.println();
        }
        public void getCountPatientsinCola()
        {
            int pacientesencola;
            try{
                pacientesencola = Us.obtenerNumeroPacientesenCola();
                System.out.println("El numero de pacientes graves en cola de espera es: "+ pacientesencola);
            }
            catch(Exception e)
            {
                System.out.println("Error:" + e.getLocalizedMessage());
            }
        }
        public void getMedMaxInc()
        {
            List<Medico> medicosmaximos = new ArrayList();
            try{
                medicosmaximos = Ms.getMedicoConMasInc();
                if(medicosmaximos.size()!= 0){
                //System.out.println("---------------------------------");
                //System.out.println("El médico con más incidencias es:");
                    
                System.out.println();    
                System.out.println("<<<< MEDICO CON MAS INCIDENCIAS >>>>");
                System.out.println("____________________________________");
                System.out.println();
                
                System.out.println("ID del medico: " + medicosmaximos.get(0).getId());
                System.out.println("Nombre: " + medicosmaximos.get(0).getNombre());
                //System.out.println(""+medicosmaximos.get(0).getId()+" "+medicosmaximos.get(0).getNombre());
                
                System.out.println();
                System.out.println("____________________________________");
                
                }
                else
                {
                    System.out.println("No hay todavía ningún médico que cumpla esas condiciones");
                }
            }
            catch(Exception e)
            {
                System.out.println("Error:" +e.getLocalizedMessage());
            }
            System.out.println();
        }
        public void getMedMaxCritic()
        {
            List<Medico> medicosmaximoscritic = new ArrayList();
            try{
                medicosmaximoscritic = Ms.getMedicoconMasCrit();
                if(medicosmaximoscritic.size()!= 0){
                
                //System.out.println("---------------------------------");
                //System.out.println("El médico con más incidencias críticas es:");
                    
                System.out.println();    
                System.out.println("<<<< MEDICO CON MAS INCIDENCIAS CRITICAS >>>>");
                System.out.println("_____________________________________________");
                System.out.println();
                
                System.out.println("ID del medico: " + medicosmaximoscritic.get(0).getId());
                System.out.println("Nombre: " + medicosmaximoscritic.get(0).getNombre());
                //System.out.println(""+medicosmaximoscritic.get(0).getId()+" "+medicosmaximoscritic.get(0).getNombre());
                
                System.out.println();
                System.out.println("_____________________________________________");
                }
                else
                {
                    System.out.println("No hay todavía ningún médico que cumpla esas condiciones");
                }
            }
            catch(Exception e)
            {
                System.out.println("Error:" +e.getLocalizedMessage());
            }
            System.out.println();
        }
        public void obtenerIncidenciasMedico() throws IOException, Exception
        {
       List<Incidencia> incidencias = null;
       System.out.println("Introduce el nombre del Medico: ");
       String nombre = br.readLine();
       incidencias = Us.obtenerIncidenciasMedico(nombre);
        try
        {
            System.out.println();
            System.out.println("<<<< LISTADO DE INCIDENCIAS DEL MEDICO "+nombre+">>>>");
            System.out.println("______________________________________________________");
            System.out.println();
            
            for (Incidencia inc : incidencias)
            {
                if(inc.getEstado().equals("HISTORICA"))
                {
                Medico med = null;
                Paciente pac = null;
                System.out.println("ID: " + inc.getIdIncidencia());
                System.out.println("Descripcion: " + inc.getDescripcion());
                System.out.println("Fecha de Inicio: " + inc.getFechaEntrada());
                System.out.println("Fecha de Atencion: " + inc.getFechaAtencion());
                System.out.println("Fecha de Finalizacion: " + inc.getFechaSalida());
                med = inc.getMedico();
                System.out.println("Medico Asignado: " + med.getNombre());
                pac = inc.getPaciente();
                System.out.println("Paciente: " + pac.getNombre());
                
                System.out.println();
                System.out.println("_____________________________________________________");
                }
            }
        }
       catch(Exception e) 
        {
            System.out.println("Error al obtener las incidencias del medico: "+ nombre +" Error"+e.getLocalizedMessage() );
        }
        System.out.println();
    }
        public void getTimeCritics() throws Exception
        {
            long media = Us.obtenerMediaCriticas();
            
            if(media == 0)
            {
                System.out.println("Todavia no hemos recibido ninguna incidencia critica.");
            }
            else
            {
                System.out.println("La media de atencion para las incidencias criticas es: "+ media/3600000 +" horas " + (media%3600000)/60000 + " minutos");
            }
        }
        public void getTimeLeves() throws Exception
        {
            long media = Us.obtenerMediaLeves();
            if(media == 0)
            {
                System.out.println("Todavia no hemos recibido ninguna incidencia leve.");
            }
            else
            {
                System.out.println("La media de atencion para las incidencias leves es: "+ media/3600000 +" horas " + (media%3600000)/60000 + " minutos");
            }
        }
        public void getTimeGraves() throws Exception
        {
            long media = Us.obtenerMediaGraves();
            if(media == 0)
            {
                System.out.println("Todavia no hemos recibido ninguna incidencia grave.");
            }
            else
            {
                System.out.println("La media de atencion para las incidencias graves es: "+ media/3600000 +" horas " + (media%3600000)/60000 + " minutos");
            }
        }

}
