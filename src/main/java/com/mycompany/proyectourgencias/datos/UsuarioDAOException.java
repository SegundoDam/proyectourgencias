/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.proyectourgencias.datos;

/**
 *
 * @author Marco Antonio
 */
public class UsuarioDAOException extends Exception{
 private static final long serialVersionUID = 1L;

	public UsuarioDAOException(String message) 
        {
		super(message);
	}
}

