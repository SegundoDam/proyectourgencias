/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.proyectourgencias.datos;

import com.mycompany.proyectourgencias.entidades.Incidencia;
import com.mycompany.proyectourgencias.entidades.Usuario;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 *  MARCO ME DABA ERROR POR LOS THROWS Y POR INCIDENCIA QUE HA DE ESTAR INCIALIZADA LO HE RETOCADO
 *  PERO NO ESTOY SEGURO QUE SEA ASI , POR LO MENOS ERA POR IR PROBANDO
 * @author Marco Antonio
 */
public class IncidenciaDAO implements IIncidenciaDAO{

    private SessionFactory sessionFactory;
    
    public IncidenciaDAO()
    {
	try
        {
            ServiceRegistry serviceRegistry;
            Configuration configuration = new Configuration();
	    configuration.configure();
	    serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	} 
	catch (Throwable e) 
        {
		System.err.println("Initial SessionFactory creation failed." + e);
		throw new ExceptionInInitializerError(e);
	}
    }
    @Override
    public void addIncidencia(Incidencia i) throws Exception
    {
        try
        {
                //abrir sesion
		Session sesion=sessionFactory.getCurrentSession();
                //iniciamos transaccion
		Transaction tx=sesion.beginTransaction();
		//hacemos los cambios
		sesion.save(i);
		tx.commit();
		//cerramos sesion
//		sesion.close();
       }
       catch(Exception e)
       {
           throw new IncidenciaDAOException("Error al agregar la incidencia");
       }
    }

    @Override
    public void updateIncidencia(Incidencia i) throws Exception  {
        Session sesion;
        
        try
        {
            sesion = sessionFactory.getCurrentSession();
            Transaction tx=sesion.beginTransaction();
            sesion.update(i);
            tx.commit();
//            sesion.close();

        } catch (Exception e)
        {
            throw new Exception("Error al modificar la incidencia");
        }
    }

    @Override
    public Incidencia getIncidencia(int idIncidencia) throws Exception 
    {
        Session sesion;
      
        Incidencia i;
        
        try{
            sesion = sessionFactory.getCurrentSession();
            Transaction tx=sesion.beginTransaction();

            i = (Incidencia) sesion.get(Incidencia.class, idIncidencia);
            tx.commit();
        }
        catch(Exception e)
        {
            throw new Exception("Error al obtener la incidencia");
        }
            return i;
    }

    @Override
    public List<Incidencia> getIncidencias() throws Exception 
    {
        Session sesion;
        //almacenará la lista
        List<Incidencia> incidencias = null;
        try
        {
            sesion=sessionFactory.getCurrentSession();
            Transaction tx=sesion.beginTransaction();

            //consulta
            Query q=sesion.createQuery("FROM Incidencia");
            incidencias=q.list();
           tx.commit();
//            sesion.close();
        }
        catch(Exception e)
        {
            throw new Exception("Error al obtener las incidencias");
       } 
        
        return incidencias; 
    }

    @Override
    public List<Incidencia> getIncidenciasMedico(int idMedico) throws Exception 
    {
        Session sesion;
        //almacenará la lista
        List<Incidencia> incidencias=null;
        List<Incidencia> incidenciasMedico = new ArrayList<Incidencia>();
        try
        {
            sesion=sessionFactory.getCurrentSession();
            Transaction tx=sesion.beginTransaction();

            //consulta
            Query q=sesion.createQuery("FROM Incidencia");
            incidencias=q.list();
              
        for(Incidencia i : incidencias)
        {
            if(i.getMedico().getId()==idMedico)
            {
                incidenciasMedico.add(i);
            }
        }
           tx.commit();
//            sesion.close();
        }
        catch(Exception e)
        {
            throw new Exception("Error al obtener las incidencias del medico con id " +idMedico);
       } 
      
        return incidenciasMedico; 
    }

    @Override
    public List<Incidencia> getIncidenciasPaciente(int idPaciente) throws Exception 
    {
        Session sesion;
        //almacenará la lista
        List<Incidencia> incidencias = null;// He inicializado a null incidencias si no daba error , no se si es asi seguro. FRAN
        List<Incidencia> incidenciasPaciente = new ArrayList<Incidencia>();
        try
        {
            sesion=sessionFactory.getCurrentSession();
            Transaction tx=sesion.beginTransaction();
   
            //consulta
            Query q=sesion.createQuery("FROM Incidencia");
            incidencias=q.list();
            for(Incidencia i : incidencias)
        {
            if(i.getPaciente().getIdPaciente()==idPaciente)
            {
                incidenciasPaciente.add(i);
            }
        }
            tx.commit();
           
//            sesion.close();
        }
        catch(Exception e)
        {
            throw new Exception("Error al obtener las incidencias del paciente con id "+idPaciente);
       } 
        
        
        return incidenciasPaciente; 
     
    }

    @Override
    public List<Incidencia> getIncidenciasActivas() throws Exception 
    {
        Session sesion;
        //almacenará la lista
        List<Incidencia> incidencias=null;
        List<Incidencia> incidenciasActivas = new ArrayList<Incidencia>();
        try
        {
            sesion=sessionFactory.getCurrentSession();
                        Transaction tx=sesion.beginTransaction();

            //consulta
            Query q=sesion.createQuery("FROM Incidencias");
            incidencias=q.list();
             for(Incidencia i : incidencias)
        {
            if(i.getEstado().equals("ACTIVA")==true)
            {
                incidenciasActivas.add(i);
            }
        }
          tx.commit();
//            sesion.close();
        }
        catch(Exception e)
        {
            throw new Exception("Error al obtener las incidencias activas");
       } 
        
       
        return incidenciasActivas; 
    }

    @Override
    public List<Incidencia> getIncidenciasHistoricas() throws Exception 
    {
        Session sesion;
        //almacenará la lista
        List<Incidencia> incidencias=null;
        List<Incidencia> incidenciasHistoricas = new ArrayList<Incidencia>();
        try
        {
            sesion=sessionFactory.getCurrentSession();
                        Transaction tx=sesion.beginTransaction();

            //consulta
            Query q=sesion.createQuery("FROM Incidencias");
            incidencias=q.list();
            for(Incidencia i : incidencias)
        {
            if(i.getTipo().equals("HISTORICA")==true)
            {
                incidenciasHistoricas.add(i);
            }
        }
           tx.commit();
//            sesion.close();
        }
        catch(Exception e)
        {
            throw new Exception("Error al obtener las incidencias historicas");
       } 
        
        
        return incidenciasHistoricas; 
    }

    @Override
    public void delIncidencia(int id) throws Exception {
        Incidencia u;
        Session sesion;
        try{
        u=this.getIncidencia(id);
        sesion=sessionFactory.getCurrentSession();
        Transaction tx=sesion.beginTransaction();
        sesion.delete(u);
        tx.commit();
//        sesion.close();
           
       }catch(Exception e){
//           throw new Exception("Error al borrar el usuario");
       }
    }
   }


