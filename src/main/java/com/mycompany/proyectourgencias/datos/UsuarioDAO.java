package com.mycompany.proyectourgencias.datos;

import com.mycompany.proyectourgencias.entidades.Usuario;
import java.util.List;
import java.util.Vector;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class UsuarioDAO implements IUsuarioDAO{
	private SessionFactory sessionFactory;
	
	public UsuarioDAO(){
		try{
                ServiceRegistry serviceRegistry;
                Configuration configuration = new Configuration();
	        configuration.configure();
	        serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
	        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
                //sessionFactory = new Configuration().configure().buildSessionFactory();
	} 
	catch (Throwable e) {
		System.err.println("Initial SessionFactory creation failed." + e);
		//throw new ExceptionInInitializerError(e);
	}
	}

    @Override
    public void addUsuario(Usuario u)// throws Exception 
    {
      try{      
        //abrir sesion
		Session sesion=sessionFactory.getCurrentSession();
                //iniciamos transaccion
		Transaction tx=sesion.beginTransaction();
		//hacemos los cambios
		sesion.save(u);
		tx.commit();
		//cerramos sesion
//		sesion.close();
       }
        catch(Exception e)
       {
//           throw new Exception("Error al agregar el usuario");
       }
    }

    @Override
    public void updateUsuario(Usuario u) //throws Exception 
    {
        Session sesion;
        
        try{
           sesion=sessionFactory.getCurrentSession();
           Transaction tx=sesion.beginTransaction();
           sesion.update(u);
           tx.commit();
//           sesion.close();
            
        }catch(Exception e){
//            throw new Exception("Error al modificar el usuario");
        }
    }

    @Override
    public void delUsuario(int idUsuario) //throws Exception 
    {
        Usuario u=null;
        Session sesion;
        try{
        u=this.getUsuario(idUsuario);
        sesion=sessionFactory.getCurrentSession();
        Transaction tx=sesion.beginTransaction();
        sesion.delete(u);
        tx.commit();
//        sesion.close();
           
       }catch(Exception e){
//           throw new Exception("Error al borrar el usuario");
       }
    }

    @Override
    public Usuario getUsuario(int idUsuario) //throws Exception 
    {
     

        Usuario u=new Usuario();
        try{
               Session sesion = sessionFactory.getCurrentSession();
                    Transaction tx=sesion.beginTransaction();
                    
            u=(Usuario) sesion.get(Usuario.class, idUsuario);
            tx.commit();
            
        }catch(Exception e){
//            throw new Exception("Error al obtener el Usuario");
        }
            return u;
        
        
    }

    @Override
    public List<Usuario> getUsuario() //throws Exception 
    {
        Session sesion;
        //almacenará la lista
        List<Usuario> usuarios=null;
        try{
            sesion=sessionFactory.getCurrentSession();
              Transaction tx=sesion.beginTransaction();
            //consulta
            Query q=sesion.createQuery("FROM Usuario");
            usuarios=q.list();
           tx.commit();
//            sesion.close();
        }catch(Exception e){
//            throw new Exception("Error al obtener los usuarios");
       } 
        
        return usuarios;
        
    }

	

}