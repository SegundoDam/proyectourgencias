/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.proyectourgencias.datos;

import com.mycompany.proyectourgencias.entidades.Incidencia;
import java.util.List;

/**
 *
 * @author Marco Antonio
 */

//(Jorge) He cambiado las IncidenciaDAOException por Exception porque es mas aconsejable
public interface IIncidenciaDAO {
    public void addIncidencia(Incidencia i)throws Exception;
    public void updateIncidencia(Incidencia i)throws Exception;
    public Incidencia getIncidencia(int idIncidencia)throws Exception;
    public List<Incidencia> getIncidencias()throws Exception;
    public List<Incidencia> getIncidenciasMedico(int idMedico)throws Exception;
    public List<Incidencia> getIncidenciasPaciente(int idPaciente)throws Exception;
    public List<Incidencia> getIncidenciasActivas()throws Exception;
    public List<Incidencia> getIncidenciasHistoricas()throws Exception;
    public void delIncidencia(int id) throws Exception;
}
