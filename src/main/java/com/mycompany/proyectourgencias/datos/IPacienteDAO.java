/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.proyectourgencias.datos;

import com.mycompany.proyectourgencias.entidades.Paciente;
import java.util.List;

/**
 *
 * @author Marco Antonio
 */
public interface IPacienteDAO 
{
    public void addPaciente(Paciente p)throws Exception;
    public void updatePaciente(Paciente p)throws Exception;
    public void delPaciente(int idPaciente)throws Exception;
    public Paciente getPacienteSeguridad(String nSegSoc) throws Exception;
    public Paciente getPaciente(int idPaciente)throws Exception;
    public List<Paciente> getPacientes()throws Exception;
    public void finalizar() throws Exception;
}
