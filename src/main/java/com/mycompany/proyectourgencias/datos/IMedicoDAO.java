/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.proyectourgencias.datos;

import com.mycompany.proyectourgencias.entidades.Medico;
import com.mycompany.proyectourgencias.entidades.Paciente;
import java.util.List;

/**
 *
 * @author Marco Antonio
 */
public interface IMedicoDAO {
    
    public void addMedico(Medico m)throws MedicoDAOException;
    public void updateMedico(Medico m)throws MedicoDAOException;
    public void delMedico(int idMedico)throws MedicoDAOException;
    public Medico getMedico(int idMedico)throws Exception;
    public List<Medico> getMedico()throws Exception;
    public List<Medico> getMedicosServicio()throws Exception;
    public Medico getMedicoNombre(String nombre) throws Exception;
    public void finalizar();
}
