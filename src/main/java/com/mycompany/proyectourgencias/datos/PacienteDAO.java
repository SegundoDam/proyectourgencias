/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.proyectourgencias.datos;

import com.mycompany.proyectourgencias.entidades.Medico;
import com.mycompany.proyectourgencias.entidades.Paciente;
import com.mycompany.proyectourgencias.entidades.Usuario;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 * Varias partes comentadas a falta del IPaciente
 * @author Fran Carpena
 */
public class PacienteDAO implements IPacienteDAO
{
    private SessionFactory sessionFactory;
    
    public PacienteDAO()
    {
        try
        {
            ServiceRegistry serviceRegistry;
                Configuration configuration = new Configuration();
	        configuration.configure();
	        serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
	        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
                //sessionFactory = new Configuration().configure().buildSessionFactory();
        } 
        catch (Exception e)
        {
            System.err.println("Initial SessionFactory creation failed." + e);
//            throw new ExceptionInInitializerError(e);
        }
    }
    
//    @Override
    @Override
    public void addPaciente(Paciente p) throws Exception
    {
        try
        {
        //abrir sesion
		Session sesion=sessionFactory.getCurrentSession();
            
		//iniciamos transaccion
		Transaction tx=sesion.beginTransaction();
		//hacemos los cambios
		sesion.save(p);
		tx.commit();
		//cerramos sesion
//		sesion.close();
       }
       catch(Exception e)
       {
           throw new PacienteDAOException("Error al agregar el paciente");
       }
        
    }

//    @Override
    @Override
    public void updatePaciente(Paciente p) throws Exception
    {
        
        
        try
        {
            Session sesion=sessionFactory.getCurrentSession();
            Transaction tx=sesion.beginTransaction();
            sesion.update(p);
            tx.commit();
//            sesion.close();

        } catch (Exception e)
        {
            throw new Exception("Error al modificar el paciente");
        }
    }
//    @Override
    @Override
    public void delPaciente(int idPaciente) throws Exception
    {
        Paciente p;
        Session sesion;
        try{
         p=new Paciente();
           Hibernate.initialize(p.getIncidencias());
         p=this.getPaciente(idPaciente);
         
        sesion=sessionFactory.getCurrentSession();
      
       
        Transaction tx=sesion.beginTransaction();
          sesion.delete(p);
        tx.commit();
//        sesion.close();
           
       }catch(Exception e){
           throw new Exception("Error al borrar paciente");
       }
    }

//    @Override
    @Override
    public Paciente getPaciente(int idPaciente) throws Exception
    {
        Session sesion;
        Paciente p;
        try{
             p = new Paciente();
            sesion =sessionFactory.getCurrentSession();
           
            Hibernate.initialize(p.getIncidencias());
            Transaction tx=sesion.beginTransaction();
            p = (Paciente) sesion.get(Paciente.class, idPaciente);
            tx.commit();
//            sesion.close();
            
        }
        catch(Exception e)
        {
            throw new Exception("Error al obtener el Paciente por la idPaciente");
        }
            return p;
        
    }

//    @Override
    @Override
    public List<Paciente> getPacientes() throws Exception
    {
        Session sesion;
        //almacenará la lista
        List<Paciente> pacientes=null;
        try
        {
            sesion = sessionFactory.getCurrentSession();
            //consulta
            Transaction tx=sesion.beginTransaction();
            Query q = sesion.createQuery("FROM Paciente");
            pacientes = q.list();
            for(Paciente p : pacientes)
            {
                Hibernate.initialize(p.getIncidencias());
            }
           
            tx.commit();
//            sesion.close();
        }
        catch(Exception e)
        {
            throw new Exception("Error al obtener los pacientes");
       } 
        
        return pacientes;
    }

    @Override
    public Paciente getPacienteSeguridad(String nSegSoc) throws Exception {
        Session sesion;
        Paciente p = new Paciente();
        String consulta = "FROM Paciente AS P WHERE P.nSegSoc = :nSegSoc";

        try{
            sesion = sessionFactory.getCurrentSession();
            Transaction tx=sesion.beginTransaction();
            Query q = sesion.createQuery(consulta).setString("nSegSoc", nSegSoc);
            List<Paciente> pacientes = q.list();
            p = pacientes.get(0);
            Hibernate.initialize(p.getIncidencias());
            tx.commit();
//            sesion.close();
            
        }
        catch(Exception e)
        {
            throw new Exception("Error al obtener el Paciente por número de la SeguridadSocial");
        }
            return p;
    }
    @Override
    public void finalizar() {
		sessionFactory.close();

	}
}
