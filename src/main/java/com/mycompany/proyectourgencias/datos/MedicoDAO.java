/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.proyectourgencias.datos;

import com.mycompany.proyectourgencias.entidades.Medico;
import com.mycompany.proyectourgencias.entidades.Paciente;
import com.mycompany.proyectourgencias.entidades.Usuario;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 *
 * @author Fran Carpena
 */

//(Jorge)Arreglados los throw Exception
public class MedicoDAO implements IMedicoDAO
{
    private SessionFactory sessionFactory;
    
    public MedicoDAO()
    {
	try
        {
            ServiceRegistry serviceRegistry;
            Configuration configuration = new Configuration();
	    configuration.configure();
	    serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
          //sessionFactory = new Configuration().configure().buildSessionFactory();
	} 
	catch (Throwable e) 
        {
		System.err.println("Initial SessionFactory creation failed." + e);
		throw new ExceptionInInitializerError(e);
	}
    }

    
    @Override
    public void addMedico(Medico m) throws MedicoDAOException
    {
        try
        {
        //abrir sesion
		Session sesion=sessionFactory.getCurrentSession();
		//iniciamos transaccion
		Transaction tx=sesion.beginTransaction();
		//hacemos los cambios
		sesion.save(m);
		tx.commit();
		//cerramos sesion
//		sesion.close();
       }
       catch(Exception e)
       {
           throw new MedicoDAOException("Error al agregar el médico");
       }
        
    }

 
    @Override
    public void updateMedico(Medico m) throws MedicoDAOException
    {
        Session sesion;
        Transaction tx;
        try
        {
     
            sesion = sessionFactory.getCurrentSession();
            tx=sesion.beginTransaction();
            sesion.update(m);
            tx.commit();
//            sesion.close();

        } catch (Exception e)
        {
            throw new MedicoDAOException("Error al modificar el médico");
        }
    }

   
    public void delMedico(int idMedico) throws MedicoDAOException
    {
        Medico m = null;
        Session sesion;
        Transaction tx;
        try
        {
            m = this.getMedico(idMedico);
            sesion = sessionFactory.getCurrentSession();
            tx = sesion.beginTransaction();
            sesion.delete(m);
            tx.commit();
//            sesion.close();

        } 
        catch (Exception e)
        {
            throw new MedicoDAOException("Error al borrar el médico");
        }
    }


    @Override
    public Medico getMedico(int idMedico) throws Exception
    {
        Session sesion;
        Medico m ;
        try{
            sesion = sessionFactory.getCurrentSession();
                        Transaction tx=sesion.beginTransaction();

            m = (Medico) sesion.get(Medico.class, idMedico);
            tx.commit();
            
        }
        catch(Exception e)
        {
            throw new Exception("Error al obtener el Médico");
        }
            return m;
        
    }

   
    @Override
    public List<Medico> getMedico() throws Exception
    {
        Session sesion;
        //almacenará la lista
        List<Medico> medicos=null;
        try
        {
            sesion=sessionFactory.getCurrentSession();
                        Transaction tx=sesion.beginTransaction();

            //consulta
            Query q=sesion.createQuery("FROM Medico");
            medicos=q.list();
           tx.commit();
//            sesion.close();
        }
        catch(Exception e)
        {
            throw new Exception("Error al obtener los médicos");
       } 
        
        return medicos;
    }
    
    // No tengo claro como debe de funcionar este método 
    // y que tendria que hacer por tanto, o sea que no estoy seguro si es correcto
  
    @Override
    public List<Medico> getMedicosServicio() throws Exception
    {
        Medico med;
        Session sesion;
        //almacenará la lista
        List<Medico> medicos=null;
        try
        {
            sesion=sessionFactory.getCurrentSession();
                        Transaction tx=sesion.beginTransaction();

            //Consulta
            Query q = sesion.createQuery("FROM Medico");
            medicos = q.list();
            //Obtenemos un iterador y recorremos la lista
            Iterator<Medico> iter = medicos.iterator();
            while(iter.hasNext())
            {
                //extraemos el objeto
                med =(Medico)iter.next();
                //System.out.println(med.getId()+"--"+med.getNombre()+"--"+med.getEstado());
            }
            tx.commit();
//            sesion.close();
            
        } catch (Exception e)
        {
            throw new Exception("Error al obtener médicos de servicio");
        }
        return medicos;
    }
   

    @Override
    public Medico getMedicoNombre(String nombre) throws Exception {
    {
        Session sesion;
       Medico m=null;
        String consulta = "FROM Medico AS M WHERE M.nombre = :nombre";

        try{
            m=new Medico();
            sesion = sessionFactory.getCurrentSession();
                        Transaction tx=sesion.beginTransaction();

            Query q = sesion.createQuery(consulta).setString("nombre", nombre);
            List<Medico> medicos= q.list();
            m= medicos.get(0);
            tx.commit();
//            sesion.close();
            
        }
        catch(Exception e)
        {
            try {
                throw new Exception("Error al obtener el Paciente por número de la SeguridadSocial");
            } catch (Exception ex) {
                Logger.getLogger(MedicoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
            return m;
     }
    }
    @Override
    public void finalizar(){
        sessionFactory.close();
    }
    public List<Medico> getMedicoMaxInc() throws Exception
    {
        Session sesion;
        List<Medico> m = new ArrayList();
       // String consulta = "from Medico as m where m.id in (SELECT medico_idMedico FROM incidencias i GROUP BY medico_idMedico having count(idIncidencia) >=all(select count(idIncidencia) from incidencias i  group by medico_idMedico))";
        String consulta = "from Medico as m where m.idMedico in (SELECT i.medico.idMedico FROM Incidencia as i GROUP BY i.medico.idMedico having count(i.idIncidencia) >=all(select count(i2.idIncidencia) from Incidencia as i2  group by i2.medico.idMedico)order by i.fechaAtencion asc)";
       try{
            
            sesion = sessionFactory.getCurrentSession();
                        Transaction tx=sesion.beginTransaction();

            Query q = sesion.createQuery(consulta);
            m= q.list();
            //Hay que coger el de la incidencia más reciente
            tx.commit();
//            sesion.close();
            
        }
        catch(Exception e)
        {
            try {
                throw new Exception("Error al obtener el médico con más incidsencias");
            } catch (Exception ex) {
                Logger.getLogger(MedicoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
            return m;
    }
    public List<Medico> getMedMaxCrit() throws Exception
    {
        Session sesion;
        List<Medico> medicosmaxcrit = new ArrayList();
       // String consulta = "from Medico as m where m.id in (SELECT medico_idMedico FROM incidencias i GROUP BY medico_idMedico having count(idIncidencia) >=all(select count(idIncidencia) from incidencias i  group by medico_idMedico))";
        String consulta = "from Medico as m where m.idMedico in (SELECT i.medico.idMedico FROM Incidencia as i WHERE i.estado = 2 AND i.fechaSalida is not null GROUP BY i.medico.idMedico order by count(idIncidencia) DESC,i.fechaAtencion DESC)";
       try{
            
            sesion = sessionFactory.getCurrentSession();
                        Transaction tx=sesion.beginTransaction();

            Query q = sesion.createQuery(consulta);
            medicosmaxcrit= q.list();
            //Hay que coger el de la incidencia más reciente
            tx.commit();
//            sesion.close();
            
        }
        catch(Exception e)
        {
            try {
                throw new Exception("Error al obtener el médico con más incidencias criticas");
            } catch (Exception ex) {
                Logger.getLogger(MedicoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
            return medicosmaxcrit;
    }
    
    
}
