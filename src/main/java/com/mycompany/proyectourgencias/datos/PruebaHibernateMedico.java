/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.proyectourgencias.datos;

import com.mycompany.proyectourgencias.entidades.Medico;
import org.hibernate.Session;
import org.hibernate.Transaction;


/**
 *
 * @author fcarpena
 */
public class PruebaHibernateMedico
{
    
   public static void main(String[] args) throws MedicoDAOException, Exception
   {
        IMedicoDAO dao = new MedicoDAO();
       
       Medico m1 = new Medico();
       m1.setId(1);
       m1.setNombre("Fran");
       m1.setEstado("DISPONIBLE");
       
       dao.addMedico(m1);

       Medico m2 = new Medico();
       m2.setId(2);
       m2.setNombre("Luis Gracia");
       m2.setEstado("OCUPADO");
       
       dao.addMedico(m2);
       ////////////////////////
       Medico pruebas = dao.getMedico(1);
       System.out.println(pruebas);
//       
//       dao.delMedico(2);
//       ////////////////////////
//       List<Medico> medicos = dao.getMedico();
//       System.out.println(medicos);
//       
//       dao.updateMedico(m1);
//       ////////////////////////
//       List<Medico> medicos2 = dao.getMedicosServicio();
//       System.out.println(medicos2);
       
   }
}
