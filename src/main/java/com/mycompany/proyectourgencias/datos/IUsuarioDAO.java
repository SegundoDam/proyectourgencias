/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.proyectourgencias.datos;

import com.mycompany.proyectourgencias.entidades.Usuario;
import java.util.List;
/**
 *
 * @author Marco Antonio
 */
public interface IUsuarioDAO {
    
    public void addUsuario(Usuario u)throws UsuarioDAOException;
    public void updateUsuario(Usuario u)throws UsuarioDAOException;
    public void delUsuario(int idUsuario)throws UsuarioDAOException;
    public Usuario getUsuario(int idUsuario)throws UsuarioDAOException;
    public List<Usuario> getUsuario()throws UsuarioDAOException;
   
    
}
