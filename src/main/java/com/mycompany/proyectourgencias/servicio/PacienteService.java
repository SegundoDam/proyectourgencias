/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.proyectourgencias.servicio;

import com.mycompany.proyectourgencias.datos.IPacienteDAO;
import com.mycompany.proyectourgencias.datos.IncidenciaDAO;
import com.mycompany.proyectourgencias.datos.PacienteDAO;
import com.mycompany.proyectourgencias.entidades.Incidencia;
import com.mycompany.proyectourgencias.entidades.Paciente;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carmen 
 */
public class PacienteService {

    
    
    private   PacienteDAO idao=new PacienteDAO();
    private   IncidenciaDAO indao = new IncidenciaDAO();
    //private final UrgenciasService us=new UrgenciasService();
    
    public PacienteDAO getIdao() {
        return idao;
    }
    public void nuevoPaciente(String nombre, String nSoc) throws Exception{
        
        List<Paciente> lista=idao.getPacientes();
        Paciente pa=new Paciente();
        try{
           for(Paciente p :  lista){
                if(p.getNombre().equals(nombre)){
                    throw new Exception("El paciente ya existe");
                }
            }
            pa.setNombre(nombre);
            pa.setnSegSoc(nSoc);
            idao.addPaciente(pa);
            
        }catch(Exception e){
            throw new Exception("Error al almacenar nuevo paciente"+e.getLocalizedMessage());
        }
       
        System.out.println("+++++++++++++++++++++++++++++++++++++++++");
        System.out.print("++ ");
        System.out.print("Nuevo paciente correctamente registrado.");
        System.out.println(" ++");
        System.out.println("+++++++++++++++++++++++++++++++++++++++++");             
          
    }
    public void actualizarPaciente(int id,String nombre,String nSoc) throws Exception{
        Paciente p;
        try{
            p=new Paciente();
            List<Paciente> lista=idao.getPacientes();
            for(Paciente pa : lista){
                if(pa.getIdPaciente() == id){
                    p=pa;
                    break;
                }
            }
            p.setNombre(nombre);
            p.setnSegSoc(nSoc);
            idao.updatePaciente(p);
            
        System.out.println("+++++++++++++++++++++++++++++++++++++++");
        System.out.print("++ ");
        System.out.print("Paciente correctamente actualizado.");
        System.out.println(" ++");
        System.out.println("+++++++++++++++++++++++++++++++++++++++");
            
            
            
        }catch(Exception e){
            throw new Exception("Error al actualizar el paciente");
        }
        
    }
    
    public void eliminarPaciente(int idPaciente) throws Exception{
        
         try{
                
            idao.delPaciente(idPaciente);
            
            
            
        }catch(Exception e){
            throw new Exception("Error al borrar el paciente");
        }
         
        System.out.println("+++++++++++++++++++++++++++++++++++");
        System.out.print("++ ");
        System.out.print("Paciente correctamente eliminado.");
        System.out.println(" ++");
        System.out.println("+++++++++++++++++++++++++++++++++++");     
        
        
    }
    
    public Paciente obtenerPaciente(int idPaciente) throws Exception{
        Paciente p;
        try{
            p = idao.getPaciente(idPaciente);
            }
            
        catch(Exception e){
            throw new Exception("Error al obtener el paciente");
        }
    
        return p;
    }
    
    public Paciente obtenerPacienteNumSegSocial(String numSegSocial) throws Exception
    {
        Paciente p;
        try
        {
            p = idao.getPacienteSeguridad(numSegSocial);
        }
        catch (Exception e)
        {
            throw new Exception("Error al obtener el paciente por el número de la Seg.Social");
        }
        
        return p;
    }
    
    public List<Paciente> obtenerTodosPaciente() throws Exception{
        List<Paciente> pacientes;
        
        try{
         pacientes=idao.getPacientes();
         if(pacientes == null){
             throw new Exception("No se encuentra pacientes en la lista");
         }
        
        }catch(Exception e){
            throw new Exception("Error al obtener todos los pacientes");
        }
                
    return pacientes;    
    }
    public List<Paciente> obtenerPacientesEnEspera() throws Exception{
       List<Incidencia> incidencias;
       List<Paciente> pacientesenespera= new ArrayList();
       Paciente p;
       try{
           incidencias = indao.getIncidencias();
           for(Incidencia i: incidencias)
           {
               if(i.getEstado().equals("ESPERA"))
               {
                   p = i.getPaciente();
                   
                   pacientesenespera.add(p);
               }
           }
       }
       catch(Exception e)
       {
           throw new Exception("Error al obteneres los pacientes en Espera");
       }
    
       
        return pacientesenespera;
    }
    public List<Paciente> obtenerPacientesEnAtencion() throws Exception{
       List<Incidencia> incidencias;
       List<Paciente> pacientesenatencion= new ArrayList();
       Paciente p;
       try{
           incidencias = indao.getIncidencias();
           for(Incidencia i: incidencias)
           {
               if(i.getEstado().equals("ACTIVA"))
               {
                   p = i.getPaciente();
                   
                   pacientesenatencion.add(p);
               }
           }
       }
       catch(Exception e)
       {
           throw new Exception("Error al obteneres los pacientes en atención");
       }
        

        
        return pacientesenatencion;
    }
    public List<Paciente> obtenerPacientesEnAtencionCritica() throws Exception{
        //Obtener incidencias
        //Miro cuales son criticas
        //y guardo el paciente de ellas.
        List<Incidencia> incidencias;
       List<Paciente> pacientesenatencioncritica= new ArrayList();
       Paciente p;
       try{
           incidencias =indao.getIncidencias();
           for(Incidencia i: incidencias)
           {
               if(i.getEstado().equals("ACTIVA") && i.getTipo().equals("CRITICA"))
               {
                   p = i.getPaciente();
                   
                   pacientesenatencioncritica.add(p);
               }
           }
       }
       catch(Exception e)
       {
           throw new Exception("Error al obteneres los pacientes en atención que estan críticos");
       }

        
        return pacientesenatencioncritica;
        
    }
    public List<Incidencia> obtenerIncidenciasPaciente(int idPaciente) throws Exception{
        Paciente p1;
        List<Incidencia> incidencias;
        List<Incidencia> incidenciasdepaciente = new ArrayList();
        try{
            p1=idao.getPaciente(idPaciente);
            incidencias=indao.getIncidencias();
            for(Incidencia i: incidencias){
                if(i.getPaciente().getIdPaciente()== p1.getIdPaciente()){
                        incidenciasdepaciente.add(i);
                   }
            }
            
          
        }catch(Exception e){
            throw new Exception("Error al obtener las incidencias del paciente");
        }
        

        return incidenciasdepaciente;
    }
}
