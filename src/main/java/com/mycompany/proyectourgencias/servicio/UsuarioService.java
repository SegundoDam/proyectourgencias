/*
 *
 * Este paquete equivale al negocio 
 */

package com.mycompany.proyectourgencias.servicio;

import com.mycompany.proyectourgencias.datos.IUsuarioDAO;
import com.mycompany.proyectourgencias.datos.UsuarioDAO;
import com.mycompany.proyectourgencias.datos.UsuarioDAOException;
import com.mycompany.proyectourgencias.entidades.TipoRol;
import com.mycompany.proyectourgencias.entidades.Usuario;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Fran Carpena ayudado por carmen
 */
public class UsuarioService 
{
    private final UsuarioDAO idao=new UsuarioDAO();

 

    public UsuarioDAO getIdao() {
        return idao;
    }
   
    
    //CONSTRUCTOR
    public void nuevoUsuario(String nombre, String usu, String pwd, String rol) throws NullPointerException, Exception
    {       
        
        Usuario u = new Usuario();
        
       
        try
        {
           //SI MIRAS SI U ES NULL, LO ES...PORQUE LO GUARDAS AL FINAL
          /* //Comprobamos que haya Usuario a guardar
            if(u == null){
                throw new NullPointerException("No hay usuario a guardar.");
            
            //Comprobamos que nombre no este vacio*/
            //¿¿Por que esto?? si tu lo que quieres es comprobar lo que te pasan
        //nombre, usuario...por parametro. si miraS el nombre del usuario es nulo porque lo guardas despues
         //if(u.getNombre().isEmpty())
            //tienes que hacer comprobaciones de lo que te pasan por parametro
           if(nombre.isEmpty()){
                throw new Exception ("El nombre no puede estar vacio");
            }
           
            //Comprobamos que usuario no este vacio
            if(usu.isEmpty()){
                throw new Exception ("El usuario no puede estar vacio");
            }
            //Comprobamos que password no este vacio
            if(pwd.isEmpty()){
                throw new Exception ("El password no puede estar vacio");
            }
            //Comprobamos que rol no este vacio
            if(rol.isEmpty()){
                throw new Exception ("El rol no puede estar vacio");
            }

            /*//Otra forma
            if(idao.getUsuario(u.getId()) != null)
                throw new Exception ("El usuario con id " + u.getId() + " ya existe. ");
            */
            //Comprobacion nombre usuario repetido
         /*   
            if(u.getUsu().equalsIgnoreCase(usu))
                throw new Exception ("El nombre de usuario (" + usu +") ya existe ");*/
            List<Usuario> usuarios = idao.getUsuario();
            for(Usuario u2 : usuarios)
            {
                if(u2.getUsu().equals(usu))
                {
                    throw new Exception("El usuario ya existe mete otro");
                }
            }
                                
            //Comprobamos que el pwd sea mayor de 5 caracteres 
            if(5 > pwd.length()){
                throw new Exception ("El password debe tener más de cinco caracteres "); 
            }
            
            //Comprobamos que el rol sea válido
            if(rol.compareToIgnoreCase(rol)!= 0){
                throw new Exception ("El rol no coincide con los existentes ");
            }
        
            u.setNombre(nombre);
            u.setUsu(usu);
            u.setPwd(pwd);
            u.setRol(rol);

            idao.addUsuario(u);	
            
        }
        catch (Exception e) 
        {
            throw new Exception("No se ha almacenado usuario: " + e.getLocalizedMessage());
	}
        
        System.out.println("+++++++++++++++++++++++++++++++++++++++++");
        System.out.print("++ ");
        System.out.print("Nuevo usuario correctamente registrado.");
        System.out.println(" ++");
        System.out.println("+++++++++++++++++++++++++++++++++++++++++");
    }
    
    public Usuario getUsuario(int id)throws Exception
    {
        try
        {
  
            return idao.getUsuario(id);
        } 
        catch (Exception e)
        {
            throw new Exception ("Hubo un error al obtener usuario con id (" + id + ")");
        }
        
        
    }

    public void eliminarUsuario(int id)throws UsuarioDAOException, UsuarioServiceException
    {
        Usuario usuario = idao.getUsuario(id);
      
        try
        {        
            //Comprobamos que exista el usuario
            if(idao.getUsuario(id) == null)
                throw new UsuarioServiceException("El usuario con id = (" + id + ") no existe.");
            //Otra forma
            List<Usuario> lu = new ArrayList();

            for (int i = 0; i < lu.size(); i++)
            {
                if(usuario.getRol()==TipoRol.SANITARIO)//Comprobamos que sea sanitario
                {
                    idao.delUsuario(id);
                }
            }
            
            if (usuario != null)// Comprobamos que el usuario sea distinto a null
            {
                idao.delUsuario(id);
            } 
            else
            {
                throw new UsuarioServiceException("No se ha encontrado usuario con id: " + usuario.getId());
            }
            
//            //Comprobamos que el usuario sea sanitario
//            if(idao.getUsuario(usuario.getRol().equalsIgnoreCase("sanitarios")))
//                throw new UsuarioServiceException("No es posible borrar usuarios distintos de sanitarios.");
        }
        catch(UsuarioServiceException use)
        {
            throw new UsuarioServiceException("No es posible borrar usuarios distintos de sanitarios ");
        }
       
        System.out.println("++++++++++++++++++++++++++++++++++");
        System.out.print("++ ");
        System.out.print("Usuario correctamente eliminado.");
        System.out.println(" ++");
        System.out.println("++++++++++++++++++++++++++++++++++");        
    }

    public void actualizarUsuario(int id,String nombre, String usu, String pwd,String rol) throws Exception
    {
        Usuario u = idao.getUsuario(id);
        List<Usuario> lu = new ArrayList();
        
        try{
            lu = idao.getUsuario();
            boolean encontrado=false;
            for(Usuario u1: lu)
            {
                if(u1.getId() == id)
                {
                    encontrado=true;
                    
                }
            }
            if(encontrado)
            {
                
                u.setNombre(nombre);
                u.setUsu(usu);
                u.setPwd(pwd);
                u.setRol(rol);
                
                idao.updateUsuario(u);
            }
            else
            {
                throw new Exception("No existe ese usuario");
            }
        }
        catch(Exception e)
        {
            e.getLocalizedMessage();
        }
        
        System.out.println("+++++++++++++++++++++++++++++++++++++");
        System.out.print("++ ");
        System.out.print("Usuario correctamente actualizado.");
        System.out.println(" ++");
        System.out.println("+++++++++++++++++++++++++++++++++++++");       
        
     
    }

        /*try
        {    
            for (int i = 0; i < lu.size(); i++)
            {
                if(idao.getUsuario(id) != null)//Comprobamos que el id sea distinto de null
                {
                    throw new Exception("No existe un Usuario con el id (" + id + ")");
                }
                
                if (lu.get(i).getId() == id)//Comprobamos que el id coincide con el buscado
                {

                    u.setNombre(nombre);
                    u.setUsu(usu);
                    u.setPwd(pwd);
                    u.setRol(rol);

                    idao.updateUsuario(u);

                    break;
                }
                
            }
        } 
        catch (Exception e)
        {
            throw new Exception ("Hubo un error al eliminar el usuario " + e.getLocalizedMessage());
        }*/
     
    

    public List<Usuario> obtenerUsuarios() throws Exception
    {
        try
        {
     
            return idao.getUsuario();
        } 
        catch (Exception e)
        {
            throw new Exception("Hubo un error al obtener los usuarios " + e.getLocalizedMessage());
        }
        
        
        
    }
    
    
    public List<Usuario> obtenerUsuariosAdministradores() throws Exception
    {
        
        List<Usuario> usuariosAdministradores = new ArrayList();
        try
        {
            List<Usuario> usuarios = idao.getUsuario();
            for (Usuario usuario : usuarios)
            {
                if (usuario.getRol()==TipoRol.ADMINISTRATIVO)
                {
                    usuariosAdministradores.add(usuario);

                }
            }

            
            
            return usuariosAdministradores;
        } 
        catch (Exception e)
        {
            throw new Exception("No se han podido obtener los usuarios administrativos");
        }
    }
    
}
