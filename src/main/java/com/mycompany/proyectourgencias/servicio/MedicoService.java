/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.proyectourgencias.servicio;

import com.mycompany.proyectourgencias.datos.MedicoDAO;
import com.mycompany.proyectourgencias.entidades.Medico;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carmen
 */
public class MedicoService {
   private  MedicoDAO mdao=new MedicoDAO();
   // public final MedicoDAO mdao2=new MedicoDAO();

    public MedicoDAO getMdao() {
        return mdao;
    }
    
    public void nuevoMedico(String nombre) throws Exception{
        
        List<Medico> lista=mdao.getMedico();
        Medico m1=new Medico();
        try{
            for(Medico m : lista){
                if(m.getNombre().equals(nombre)){
                    throw new Exception("El medico ya existe" );
                    
                }
            }
            m1.setNombre(nombre);
            m1.setEstado("FUERA_SERV");
            mdao.addMedico(m1);
        }catch(Exception e){
            throw new Exception("No se ha introducido correctamente el medico" + e.getLocalizedMessage());
        }
        
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.print("++ ");
        System.out.print("Nuevo medico correctamente registrado.");
        System.out.println(" ++");
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++");
    }
    public void actualizarMedico(int id,String nombre,String estado) throws Exception{
        
        try{
            List<Medico> lista=mdao.getMedico();
            Medico m1=new Medico();
            for(Medico m : lista){
                if(m.getId()==id){
                    m1=m;
                    break;
                }
            }
            m1.setNombre(nombre);
            m1.setEstado(estado);
            mdao.updateMedico(m1);
            
        }catch(Exception e){
            throw new Exception("No se ha actualizado correctamente el medico");
        }
        
        System.out.println("++++++++++++++++++++++++++++++++++++");
        System.out.print("++ ");
        System.out.print("Medico correctamente actualizado.");
        System.out.println(" ++");
        System.out.println("++++++++++++++++++++++++++++++++++++");
    }
    
    
    public void eliminarMedico(int id) throws Exception{
        List<Medico> lista=mdao.getMedico();
        try{
            boolean noencontrado=true;
            for(Medico m: lista){
                if(m.getId() == id){
                    noencontrado=false;
                }
            }
            if(noencontrado)
            {
                throw new Exception("No existe el medico");
            }
            
        mdao.delMedico(id);
        }catch(Exception e){
            throw new Exception("No se ha eliminado el medico" + e.getLocalizedMessage());
        }
        
        System.out.println("+++++++++++++++++++++++++++++++++");
        System.out.print("++ ");
        System.out.print("Medico correctamente eliminado.");
        System.out.println(" ++");
        System.out.println("+++++++++++++++++++++++++++++++++");
    }
    
    public List<Medico> obtenerTodosMedicos() throws Exception{
      
        try{
            
          List<Medico> lista=mdao.getMedico();
          if(lista == null){
              throw new Exception("No hay medicos en la lista");
          }
             
         
        return lista;
          
        }catch(Exception e){
            throw new Exception("No se han podido obtener todos los medicos");
        }
             
        
    }
    
    //obtiene medicos que estan libres u ocupados
    public List<Medico> obtenerMedicosServicio() throws Exception{
     
        List<Medico> lista=mdao.getMedico();
       
        List<Medico> disponible=new ArrayList();
               
       try{
           for(Medico m : lista){
              
               if(m.getEstado().equals("DISPONIBLE")){
                   disponible.add(m);

               }
             
          }
           
       }catch(Exception e){
           throw new Exception("No se han podido obtener los medicos disponibles"+ e.getLocalizedMessage());
       }
       
  
    return disponible;
    }
    
    public Medico getMedicoNombre(String nombre) throws Exception{
        Medico m;
        try{
            m = mdao.getMedicoNombre(nombre);
        }
        catch(Exception e)
        {
            throw new Exception("Fallo al recuperar el paciente por el nombre");
        }
        

        return m;
    }
    public List<Medico> getMedicoConMasInc() throws Exception{
        List<Medico> medicos = new ArrayList();
        try
        {
         medicos = mdao.getMedicoMaxInc();
        }
        catch(Exception e)
        {
            throw new Exception("Error al obtener el médico con mas incidencias"+ e.getLocalizedMessage());
        }
        

        
        return medicos;
    }
    public List<Medico> getMedicoconMasCrit() throws Exception{
        List<Medico> medicosmascrit = new ArrayList();
        try
        {
         medicosmascrit = mdao.getMedMaxCrit();
        }
        catch(Exception e)
        {
            throw new Exception("Error al obtener el médico con mas incidencias críticas"+ e.getLocalizedMessage());
        }
    
        
        return medicosmascrit;
    }
}
