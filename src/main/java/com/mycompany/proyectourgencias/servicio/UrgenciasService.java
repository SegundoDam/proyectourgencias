/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.proyectourgencias.servicio;

import com.mycompany.proyectourgencias.datos.IncidenciaDAO;
import com.mycompany.proyectourgencias.datos.MedicoDAO;
import com.mycompany.proyectourgencias.datos.PacienteDAO;
import com.mycompany.proyectourgencias.entidades.Incidencia;
import com.mycompany.proyectourgencias.entidades.Paciente;
import com.mycompany.proyectourgencias.entidades.Medico;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fran Carpena
 */
public final class UrgenciasService 
{
    //ATRIBUTOS
    //Creamos las instancias pertinentes
    //PacienteDAO pacdao = new PacienteDAO();
    //MedicoDAO meddao = new MedicoDAO();
    private static  IncidenciaDAO incdao = new IncidenciaDAO();
    private PacienteService ps=new PacienteService();

    public IncidenciaDAO getIncdao() {
        return incdao;
    }
    
    
    Queue<Incidencia> incidenciasGraves = new LinkedList();
    Queue<Incidencia> incidenciasLeves = new LinkedList();
    
    MedicoService ms = new MedicoService();
    public UrgenciasService() {
        try {
            inicializarColas();
        } catch (Exception ex) {
            Logger.getLogger(UrgenciasService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
    
    //METODOS
    /*
    A partir de idPaciente y descripcion construimos una incidencia nueva cada vez
    y asignamos un medico. Si hay uno disponible a ese y fechaEntrada y fechaAtencion
    la del sistema. Si no hay medico disponible asginamos fechaEntrada (la del sistema)
    Incidencia pasa a ESPERA y la insertamos en la cola LEVES, obteniendo e insertando
    el dato numeroOrden que sera el que tenga el paciente en la cola. En cualquier otro
    caso la incidencia se guarda en la base de datos.
    */
    public void registrarIncidenciaLeve(int idPaciente, String descripcion) throws Exception
    {
        Incidencia inc= new Incidencia();
        Medico m1 = null;
        Paciente p1 = null;
        
        String d, h;
        
        try
        {
            //realizamos varias comprobaciones para asegurarnos que lo introducido es correcto
            if(idPaciente <=0){
                throw new UrgenciasException("El id de Paciente ha de ser mayor de 0.");
            }    
            if(descripcion.equals("")){
                throw new UrgenciasException("La descripción no puede estar vacia.");
            }       
           
            
                                
            //obtenemos medicos de servicio
          List<Medico> listaMedDisp=null;
           listaMedDisp=ms.obtenerMedicosServicio();//Obtenemos lista con medicosServicio
           inc.setTipo("LEVE");
            p1 = ps.getIdao().getPaciente(idPaciente);//recogemos el paciente con su idPaciente
            inc.setPaciente(p1);//asignamos incidenciaLeve a este Paciente
            inc.setDescripcion(descripcion);//registramos descripción a incidencia
                     
            //tenemos médico disponible
           if(!listaMedDisp.isEmpty())
           {
               for (Medico m : listaMedDisp)//recorremos toda la lista de medicos
               {
                   if(m.getEstado().equals("DISPONIBLE"))//si equivale a DISPONIBLE
                   {
                       m1 = m;
                      break;
                   }
               }
           }
          //hay medico
           if(m1!=null){
                        m1.setEstado("OCUPADO");//cambiamos el estado del médico
                     
                        
                        inc.setMedico(m1);//asignamos médico a incidencia
                       // meddao.updateMedico(m1); //ACTUALIZO MEDICO
                        ms.actualizarMedico(m1.getId(), m1.getNombre(), "OCUPADO");
                        inc.setEstado("ACTIVA");//pasa a incidencia "ACTIVA"
                            try
                             {
                                 Date today = Calendar.getInstance().getTime();
                                 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                 d = sdf.format(today);
                                 inc.setFechaEntrada(today);
                                 inc.setFechaAtencion(today);

                             }
                             catch(Exception efecha)
                             {
                                 throw new Exception("El formato de fecha no es válido.");
                             }
                         
                           // incdao.addIncidencia(inc); //AÑADIMOS LA INCIDENCIA
                        }else //no tenemos médico disponible dejamos incidencia en ESPERA
                            {
                               try{
                                 Date today = Calendar.getInstance().getTime();
                                 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                 d = sdf.format(today);
                                 inc.setFechaEntrada(today);
                               

                                    }
                                    catch(Exception efecha)
                                        {
                                            throw new Exception("El formato de fecha no es válido.");
                                        }

                                //incidencia "ESPERA"
                                inc.setEstado("ESPERA");
                                //obtener el utlimo numero en espera
                                int num=0;
                                try{
                                //obtengo las incidencias
                                List<Incidencia> incid=incdao.getIncidencias();
                                for(Incidencia i : incid){ //las recorro
                                    //compruebo si el estado de la incidencia es leve
                                    if(i.getNumEspera()>=num){ //si el numero que obtengo es mayor o igual que el numero de espera
                                      num= i.getNumEspera()+1; //lo añado y le sumo uno
                                      //no habrá pacientes con numero de espera 0, ya que 0 es estar atendido.

                                    }
                                }
                                }catch(Exception e){
                                    throw new Exception("NO se ha podido obtener el numero de cola");
                                }
                                //insertando numOrden (número del paciente en la cola)
                                inc.setNumEspera(num);

                                //insertamos en cola "LEVES", 
                                inc.setTipo("LEVE");
                                incidenciasLeves.add(inc);
                               // incdao.addIncidencia(inc);
                              

                            }
           
        } 
        catch (Exception e)
        {
            throw new Exception ("Error al registrar incidencia leve." + e.getLocalizedMessage());
        }
       
               incdao.addIncidencia(inc);
               
               System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++");
               System.out.print("++ ");
               System.out.print("Incidencia leve correctamente registrada.");
               System.out.println(" ++");
               System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++");
     }
    
    /*
    Igual que en LEVES pero asignando a la cola de GRAVES
    */
    public void registrarIncidenciaGrave(int idPaciente, String descripcion) throws Exception
    {
        
        Incidencia inc= new Incidencia();
        Medico m1 = null;
        Paciente p1 = null;
        
        String d, h;
        
        try
        {
            //realizamos varias comprobaciones para asegurarnos que lo introducido es correcto
            if(idPaciente <=0){
                throw new UrgenciasException("El id de Paciente ha de ser mayor de 0.");
            }    
            if(descripcion.equals("")){
                throw new UrgenciasException("La descripción no puede estar vacia.");
            }       
                              
            //obtenemos medicos de servicio
          List<Medico> listaMedDisp=null;
           listaMedDisp=ms.obtenerMedicosServicio();//Obtenemos lista con medicosServicio
           inc.setTipo("GRAVE");
            p1 = ps.getIdao().getPaciente(idPaciente);//recogemos el paciente con su idPaciente
            inc.setPaciente(p1);//asignamos incidenciaLeve a este Paciente
            inc.setDescripcion(descripcion);//registramos descripción a incidencia
                     
            //tenemos médico disponible
           if(!listaMedDisp.isEmpty())
           {
               for (Medico m : listaMedDisp)//recorremos toda la lista de medicos
               {
                   if(m.getEstado().equals("DISPONIBLE"))//si equivale a DISPONIBLE
                   {
                       m1 = m;
                      break;
                   }
               }
           }
          //hay medico
           if(m1!=null){
                        m1.setEstado("OCUPADO");//cambiamos el estado del médico
                       
                        
                        inc.setMedico(m1);//asignamos médico a incidencia
                           ms.actualizarMedico(m1.getId(), m1.getNombre(), "OCUPADO");
                        inc.setEstado("ACTIVA");//pasa a incidencia "ACTIVA"
                            try
                             {
                                 Date today = Calendar.getInstance().getTime();
                                 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                 d = sdf.format(today);
                                 inc.setFechaEntrada(today);
                                 inc.setFechaAtencion(today);

                             }
                             catch(Exception efecha)
                             {
                                 throw new Exception("El formato de fecha no es válido.");
                             }
                           // incdao.addIncidencia(inc); //AÑADIMOS LA INCIDENCIA
                        }else //no tenemos médico disponible dejamos incidencia en ESPERA
                            {
                               try{
                                 Date today = Calendar.getInstance().getTime();
                                 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                 d = sdf.format(today);
                                 inc.setFechaEntrada(today);
                               

                                    }
                                    catch(Exception efecha)
                                        {
                                            throw new Exception("El formato de fecha no es válido.");
                                        }

                                //incidencia "ESPERA"
                                inc.setEstado("ESPERA");
                                //obtener el utlimo numero en espera
                                int num=1;
                                try{
                                //obtengo las incidencias
                                List<Incidencia> incid=incdao.getIncidencias();
                                for(Incidencia i : incid){ //las recorro
                                    if(i.getNumEspera()>=num){ //si el numero que obtengo es mayor o igual que el numero de espera
                                      num= i.getNumEspera()+1; //lo añado y le sumo uno
                                      //no habrá pacientes con numero de espera 0, ya que 0 es estar atendido.

                                    }
                                }
                                }catch(Exception e){
                                    throw new Exception("NO se ha podido obtener el numero de cola");
                                }
                                //insertando numOrden (número del paciente en la cola)
                                inc.setNumEspera(num);

                               
                                inc.setTipo("GRAVE");
                                //insertamos en la cola de graves
                                incidenciasGraves.add(inc);
                                System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++");
                                System.out.print("++ ");
                                System.out.print("Incidencia grave correctamente registrada.");
                                System.out.println(" ++");
                                System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++");

                            }
           
        } 
        catch (Exception e)
        {
            throw new Exception ("Error al registrar incidencia Grave.");
        }
          incdao.addIncidencia(inc);
    }
    
    /*
    Crea incidencia asignandola a idPaciente,si este no existe tramitará la incidencia
    igualmente asignando NULL a paciente posteriormente se modificará la incidencia
    para asignar paciente válido. Una vez hecho pasará a incidencia ACTIVA.Obtendremos 
    fecha atención y de entrada del sistema. En caso de no haber médico lanzará excepción
    e introduciremos de nuevo la incidencia cuando haya médico disponible.
    */
    public void registrarIncidenciaCritica(int idPaciente, String descripcion) throws Exception
    {
        
        Incidencia inc= new Incidencia();
        Medico m1 = null;
        Paciente p1 = null;
        
        String d, h;
        
        try
        {
            //realizamos varias comprobaciones para asegurarnos que lo introducido es correcto
            if(idPaciente <=0){
                throw new UrgenciasException("El id de Paciente ha de ser mayor de 0.");
            }    
            if(descripcion.equals("")){
                throw new UrgenciasException("La descripción no puede estar vacia.");
            }       
           
            
                                
            //obtenemos medicos de servicio
          List<Medico> listaMedDisp=null;
           listaMedDisp=ms.obtenerMedicosServicio();//Obtenemos lista con medicosServicio
           inc.setTipo("CRITICA");
            p1 = ps.getIdao().getPaciente(idPaciente);//recogemos el paciente con su idPaciente
            inc.setPaciente(p1);//asignamos incidenciaLeve a este Paciente
            inc.setDescripcion(descripcion);//registramos descripción a incidencia
                     
            //tenemos médico disponible
           if(!listaMedDisp.isEmpty())
           {
               for (Medico m : listaMedDisp)//recorremos toda la lista de medicos
               {
                   if(m.getEstado().equals("DISPONIBLE"))//si equivale a DISPONIBLE
                   {
                       m1 = m;
                      break;
                   }
               }
           }
          //hay medico
           if(m1!=null){
                        m1.setEstado("OCUPADO");//cambiamos el estado del médico
                       //LO ACTUALIZO EN LA BASE DE DATOS
                       ms.getMdao().updateMedico(m1);
                        inc.setMedico(m1);//asignamos médico a incidencia
                        inc.setEstado("ACTIVA");//pasa a incidencia "ACTIVA"
                            try
                             {
                                 Date today = Calendar.getInstance().getTime();
                                 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                 d = sdf.format(today);
                                 inc.setFechaEntrada(today);
                                 inc.setFechaAtencion(today);

                             }
                             catch(Exception efecha)
                             {
                                 throw new Exception("El formato de fecha no es válido.");
                             }
                           // incdao.addIncidencia(inc); //AÑADIMOS LA INCIDENCIA
                        }else //no tenemos médico disponible le asignamos un medico de la cola de leves y la incidencia que se queda sin
                                //medico la metemos en la cola de leves.
                            {
                               try{
                                 Date today = Calendar.getInstance().getTime();
                                 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                 d = sdf.format(today);
                                 inc.setFechaEntrada(today);
                                  inc.setFechaAtencion(today);
                               

                                    }
                                    catch(Exception efecha)
                                        {
                                            throw new Exception("El formato de fecha no es válido.");
                                        }

                                //incidencia "ESPERA"
                                //inc.setEstado("ESPERA");
                                //obtener el utlimo numero en espera
                                int num=1;
                                try{
                                //obtengo las incidencias
                                List<Incidencia> incid=incdao.getIncidencias();
                                for(Incidencia i : incid){ //las recorro
                                    if(i.getNumEspera()>=num){ //si el numero que obtengo es mayor o igual que el numero de espera
                                      num= i.getNumEspera()+1; //lo añado y le sumo uno
                                      //no habrá pacientes con numero de espera 0, ya que 0 es estar atendido.

                                    }
                                }
                                }catch(Exception e){
                                    throw new Exception("NO se ha podido obtener el numero de cola");
                                }
                                //insertando numOrden (número del paciente en la cola)
                                inc.setNumEspera(num);

                                
                                inc.setTipo("CRITICA");
                              //obtenemos la cola de leves y le asignamos un medico, la incidencia pasa a la cola de leves 
                               //a la espera de ser atendida
                                List<Incidencia> listaActivas=this.obtenerActivas();
                                //recorro la lista y cojo una incidencia
                                Incidencia leveNoAtendida=new Incidencia();
                                for(Incidencia i: listaActivas){
                                    leveNoAtendida=i;
                                    leveNoAtendida.setEstado("ESPERA");
                                    incdao.updateIncidencia(leveNoAtendida);
                                     incdao.delIncidencia(i.getIdIncidencia());
                                    incidenciasLeves.add(leveNoAtendida);
                                    break;
                                }
                                
                                //obtengo el medico
                                Medico m=leveNoAtendida.getMedico();
                                 if (m==null)
                                {
                                    //no se ha podido subir
                                    System.out.println("NO HAY MEDICOS, BUSCA OTRO HOSPITAL, RAPIDO!");
                                    throw new Exception();
                                }
                                //le asigno el medico a la incidencia critica
                                inc.setMedico(m);
                                inc.setEstado("ACTIVA"); //cambio su estado a activa
                                //añado la incidencia a la cola de leves
                             
                    }
           
        } 
        catch (Exception e)
        {
            throw new Exception ("Error al registrar incidencia Grave.");
        }
          incdao.addIncidencia(inc);
          
          System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
          System.out.print("++ ");
          System.out.print("Incidencia critica correctamente registrada.");
          System.out.println(" ++");
          System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
    }
    
    /*
    Finaliza una incidencia una vez atendida,modifica el tipo de incidencia a histórica
    y guarda la fecha de sistema en fecha salida.
    Posteriormente obtiene una incidencia de la cola de graves y se la asigna al médico
    que ha quedado libre, en caso de que no hayan le asigna de la cola de leves y si 
    tampoco hubiera pasa el médico a DISPONIBLE.
    */
    public void finalizarIncidencia(int idIncidencia)throws NullPointerException,Exception
    {
        Medico med = new Medico();
        Incidencia i1 = new Incidencia();
        
        
        String d;
        
        try
        {
            //obtenemos incidencias y actualizamos estado a HISTORICA
            List<Incidencia> listaIncidencias = incdao.getIncidencias();
                   
            for(Incidencia i : listaIncidencias)
            {
                if(i.getIdIncidencia()== idIncidencia)
                {
                    i1 = i;
                    break;
                }
            }
          
            i1.setEstado("HISTORICA");
          
            //guarda la fecha de sistema en fecha de salida
            try
            {
                Date today = Calendar.getInstance().getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                d = sdf.format(today);
                 i1.setFechaSalida(today);
                
                               
            }
            catch(Exception efecha)
            {
                throw new Exception("El formato de fecha no es válido.");
            }
            //obtengo el medico de la incidencia
           
            //incdao.addIncidencia(i1);
            Medico mDisponible=i1.getMedico();
            mDisponible.setEstado("DISPONIBLE");
            //actualizo su estado a disponible
            //meddao.updateMedico(mDisponible);
            
            ms.getMdao().updateMedico(mDisponible);
            
           
            //obtengo la lista de medicos disponibles
            List<Medico> medicos=ms.obtenerMedicosServicio();
            //recorro la lista de medicos para obtener el medico disponible
            Medico m1 = null;
            for(Medico m : medicos){
                if(m.getEstado().equals("DISPONIBLE")){
                    m1=m;
                    break;
                }
            }
             //obtengo la cola de Graves
            List<Incidencia> listaGraves=this.obtenerColaGraves();
            //obtengo la lista de leves
            List<Incidencia>listaLeves=this.obtenerColaLeves();
            if(!listaGraves.isEmpty()){
                Incidencia i=incidenciasGraves.element();
                i.setMedico(m1);
                 //incdao.updateIncidencia(i);
                 Paciente p=i.getPaciente();
                registrarIncidenciaGrave(p.getIdPaciente(), i.getDescripcion());
                incdao.delIncidencia(i.getIdIncidencia());
                 incdao.updateIncidencia(i1);
              incidenciasGraves.remove(i);
            }else if(!listaLeves.isEmpty()){
                Incidencia i=incidenciasLeves.element();
                i.setMedico(m1);
                 //incdao.updateIncidencia(i);
                 Paciente p=i.getPaciente();
                registrarIncidenciaLeve(p.getIdPaciente(), i.getDescripcion());
                incdao.delIncidencia(i.getIdIncidencia());
                incdao.updateIncidencia(i1);
                incidenciasLeves.remove(i);
               
               
            }else if(listaGraves.isEmpty() && listaLeves.isEmpty()){
                 ms.actualizarMedico(mDisponible.getId(), mDisponible.getNombre(), "DISPONIBLE");
                 ms.getMdao().updateMedico(mDisponible);//actualizo el medico a disponible
                  incdao.updateIncidencia(i1);
                  incidenciasGraves.clear();
                  incidenciasLeves.clear();
                         
            }
            
         
//          List<Incidencia> li = null;
//          li = incdao.getIncidenciasActivas();
//          for (Incidencia li1 : li)
//          {
//              if(li1.getIdIncidencia() != idIncidencia)
//              {
//                  throw new Exception("No existe ese id de Incidencia.");
//              }
//          }
//          inc.setEstado("HISTORICA");
      
            //obtiene incidencia de la cola GRAVES
           /* try
            {
                if(incidenciasGraves != null)
                {
                    Incidencia i = incidenciasGraves.element();
                  
                    //se la asigna al médico que quedo libre
                    i.setMedico(med);
                }
              
                if(incidenciasGraves == null)
                {
                    if(incidenciasLeves != null)
                    {
                        Incidencia i = incidenciasLeves.element();
                      
                        //asignamos médico
                        i.setMedico(med);
                    }
                    else
                    {
                        //pasa estado del médico a DISPONIBLE
                        med.setEstado("DISPONIBLE"); 
                    }    
                }
            } 
            catch (RuntimeException re)
            {
                throw new Exception ("No hay incidencias.");
            }
        } */
        }catch (Exception e)
        {
            throw new Exception("Error al finalizar la incidencia. ");
        }  
    System.out.println("++++++++++++++++++++++++++++++++++++++++++");
    System.out.print("++ ");
    System.out.print("Incidencia correctamente finalizada.");
    System.out.println(" ++");
    System.out.println("++++++++++++++++++++++++++++++++++++++++++");
  }
    
//    List<Incidencia> li = incdao.getIncidenciasActivas();//vemos incidencias activas
//    List<Medico> lm = meddao.getMedico();

    /* igual que finalizarIncidencia pero si el médico finaliza turno en lugar
    de asignarle nueva incidencia pasa a FUERA_SERV
    */
    public void finalizarIncidenciaFinTurno(int idIncidencia) throws Exception
    {
          Medico med = new Medico();
        Incidencia i1 = new Incidencia();
        
        
        String d;
        
        try
        {
            //obtenemos incidencias y actualizamos estado a HISTORICA
            List<Incidencia> listaIncidencias = incdao.getIncidencias();
                   
            for(Incidencia i : listaIncidencias)
            {
                if(i.getIdIncidencia()== idIncidencia)
                {
                    i1 = i;
                    break;
                }
            }
          
            i1.setEstado("HISTORICA");
          
            //guarda la fecha de sistema en fecha de salida
            try
            {
                Date today = Calendar.getInstance().getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                d = sdf.format(today);
                 i1.setFechaSalida(today);
                
                               
            }
            catch(Exception efecha)
            {
                throw new Exception("El formato de fecha no es válido.");
            }
          
            incdao.updateIncidencia(i1);
            incdao.addIncidencia(i1);
            //obtengo el medico de la incidencia
            Medico mDisponible=i1.getMedico();
            ms.actualizarMedico(mDisponible.getId(), mDisponible.getNombre(), "FUERA_SERV"); //actualizo el medico a fuera de servicio
            
        } 
        catch (Exception e)
        {
            throw new Exception("Error al finalizar incidencias fin de turno. ");
        }
        
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.print("++ ");
        System.out.print("Incidencia por fin de turno finalizada correctamente.");
        System.out.println(" ++");
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    }
    
    
    public void comenzarTurno(int idMedico)throws UrgenciasException, Exception
    {
        Medico m1 =ms.getMdao().getMedico(idMedico);
        
        boolean estado=false;
                
            if(m1.getEstado().equals("FUERA_SERV"))// comprobamos que ese id existe
            {
                estado =true;
            }
        
        if(estado)
        {
            m1.setEstado("DISPONIBLE");// asignamos estado DISPONIBLE
            //actualizo el medico en la bbdd
            ms.actualizarMedico(m1.getId(), m1.getNombre(), "DISPONIBLE");
            //cogemos una incidencia...
            //de la cola de graves...
            if(!incidenciasGraves.isEmpty()){
                Incidencia i;
                i=incidenciasGraves.element();
                i.setMedico(m1);
                Paciente p=i.getPaciente();
                registrarIncidenciaGrave(p.getIdPaciente(), i.getDescripcion());
                incidenciasGraves.remove(i);
                incdao.delIncidencia(i.getIdIncidencia());
            }else  if(!incidenciasLeves.isEmpty()){
                Incidencia i;
                i=incidenciasLeves.element();
                i.setMedico(m1);
                Paciente p=i.getPaciente();
                registrarIncidenciaLeve(p.getIdPaciente(), i.getDescripcion());
                incidenciasGraves.remove(i);
                incdao.delIncidencia(i.getIdIncidencia());
            }
        }
        else
        {
            throw new UrgenciasException("Error al comenzar turno el médico(UrgenciasService)");
        }
        
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++");
        System.out.print("++ ");
        System.out.print("Comienzo de turno correctamente registrado.");
        System.out.println(" ++");
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++");
    }
    
    
    public void finalizarTurno(int idMedico)throws UrgenciasException, Exception
    {
      List<Medico> listaMedicos = ms.getMdao().getMedico();
        Medico m1 = ms.getMdao().getMedico(idMedico);
       
        boolean estado=false;
        for (Medico m : listaMedicos)
        {
            if(m.getId() == idMedico && m.getEstado().equals("DISPONIBLE"))// comprobamos que ese id existe
            {
                estado =true;
            }

        }
        if(estado==true)
        {
            m1.setEstado("FUERA_SERV");// asignamos estado FUERA DE SERVICIO
            ms.actualizarMedico(m1.getId(), m1.getNombre(), "FUERA_SERV");
        }
        else
        {
            throw new UrgenciasException("Error al terminar turno el médico(UrgenciasService)");
        }
        
        System.out.println("++++++++++++++++++++++++++++++++");
        System.out.print("++ ");
        System.out.print("Turno finalizado correctamente.");
        System.out.println(" ++");
        System.out.println("++++++++++++++++++++++++++++++++");
    }
    
    public void inicializarColas() throws Exception
    {
        try
        {
          //Lista que contiene todas las incidencias
            List<Incidencia> lista=incdao.getIncidencias();
           //recorro la lista de incidencias
            for(Incidencia i : lista){
                //si el tipo de la incidencia es leve, lo añado a la cola de leves
                if(i.getTipo().equals("LEVE") && i.getEstado().equals("ESPERA")){
                    incidenciasLeves.add(i);
                    
                }else if(i.getTipo().equals("GRAVE") && i.getEstado().equals("ESPERA")){
                    incidenciasGraves.add(i);
                }
            }
        } 
        catch (Exception exception)
        {
            throw new Exception ("Error: no ha sido posible inicializar las colas.");
        }
        
        System.out.println("++++++++++++++++++++++++++++++++++");
        System.out.print("++ ");
        System.out.print("Colas inicializadas correctamente.");
        System.out.println(" ++");
        System.out.println("++++++++++++++++++++++++++++++++++");
    }
    
    public List<Incidencia> obtenerActivas()throws Exception
    {
       List<Incidencia> incidencias = null; 
       incidencias = incdao.getIncidencias();
       List<Incidencia> activas=new ArrayList();
       
        try
        {
            for (Incidencia inc : incidencias)
            {
                if(inc.getEstado().equals("ACTIVA"))
                {
                    // si su estado es ACTIVA lo añadimos a la lista
                   activas.add(inc);
                }
            }
        }
       catch(Exception e) 
        {
            throw new Exception("Error al obtener incidencias activas."); 
        }
       
         
        return activas;
       
       
    }
    
    //obtenemos todas las incidencias que estan en estado de ESPERA
    public List<Incidencia> obtenerEspera() throws Exception
    {
         List<Incidencia> incidencias = null; 
       incidencias = incdao.getIncidencias();
       List<Incidencia> activas=new ArrayList();
       
        try
        {
            for (Incidencia inc : incidencias)
            {
                if(inc.getEstado().equals("ESPERA"))
                {
                    // si su estado es ACTIVA lo añadimos a la lista
                   activas.add(inc);
                }
            }
        }
       catch(Exception e) 
        {
            throw new Exception("Error al obtener incidencias activas."); 
        }
        
        
       return activas;
    }
    
    public List<Incidencia> obtenerColaLeves() throws Exception
    {
      List<Incidencia> leves;
      try{
       
        //CREO UNA LISTA DE INCIDENCIAS LEVES
       leves=new ArrayList();
       for(Incidencia i : incidenciasLeves){
           //si el estado es en espera
           if(i.getEstado().equals("ESPERA") && i.getTipo().equals("LEVE")){
               leves.add(i);
           }
           
       }
        
        }catch(Exception e){
            throw new Exception("No se ha podido recuperar la cola de leves");
        }
       

       
        return leves;
    }
    
    public List<Incidencia> obtenerColaGraves() throws Exception
    {
      List<Incidencia> graves;
      try{
       
        //CREO UNA LISTA DE INCIDENCIAS LEVES
       graves=new ArrayList();
       for(Incidencia i : incidenciasGraves){
           //si el estado es en espera
           if(i.getEstado().equals("ESPERA") && i.getTipo().equals("GRAVE")){
               graves.add(i);
           }
           
       }
        
        }catch(Exception e){
            throw new Exception("No se ha podido recuperar la cola de leves");
        }

       
        return graves;
    }
    
    //AÑADO EL METODO PARA ACTUALIZAR UNA INCIDENCIA
    public void actualizarIncidencia(Incidencia i,int id,String estado) throws Exception{
         i.setIdIncidencia(id);
         i.setEstado(estado);
         incdao.updateIncidencia(i);
        
    }
    //Obtener incidencias historicas
    public List<Incidencia> obtenerHistoricas()throws Exception
    {
       List<Incidencia> incidencias = null; 
       incidencias = incdao.getIncidencias();
       List<Incidencia> historicas=new ArrayList();
       
        try
        {
            for (Incidencia inc : incidencias)
            {
                if(inc.getEstado().equals("HISTORICA"))
                {
                    // si su estado es ACTIVA lo añadimos a la lista
                   historicas.add(inc);
                }
            }
        }
       catch(Exception e) 
        {
            throw new Exception("Error al obtener incidencias históricas."); 
        }

       return historicas;
    }
    public List<Incidencia> obtenercriticasatendidas()throws Exception
    {
        List<Incidencia> incidencias = null;
        List<Incidencia> criticasatendidas = new ArrayList();
        try
        {
            incidencias = incdao.getIncidencias();
            for (Incidencia inc : incidencias)
            {
                if(inc.getEstado().equals("ACTIVA") && inc.getTipo().equals("CRITICA"))
                {
                    // si su estado es ACTIVA lo añadimos a la lista
                   criticasatendidas.add(inc);
                }
            }
        }
       catch(Exception e) 
        {
            throw new Exception("Error al obtener incidencias atendidas y son críticas."); 
        }

        return criticasatendidas;
               
    }
    public List<Incidencia> obtenercriticashistoricas()throws Exception
    {
        List<Incidencia> incidencias = null;
        List<Incidencia> criticashistoricas = new ArrayList();
        try
        {
            incidencias = incdao.getIncidencias();
            for (Incidencia inc : incidencias)
            {
                
                if(inc.getEstado().equals("HISTORICA") && inc.getTipo().equals("CRITICA"))
                {
                    // si su estado es ACTIVA lo añadimos a la lista
                   criticashistoricas.add(inc);
                }
            }
        }
       catch(Exception e) 
        {
            throw new Exception("Error al obtener incidencias historicas y críticas."); 
        }
        

        return criticashistoricas;
    }
    public List<Incidencia> obtenerIncidenciasMedico(String nombre)throws Exception
    {
       List<Incidencia> incidencias = null; 
       incidencias = incdao.getIncidencias();
       List<Incidencia> incidenciasmedico=new ArrayList();
       
        try
        {
            Medico med = null;
            for (Incidencia inc : incidencias)
            {
                med=ms.getMdao().getMedicoNombre(nombre);
                if(inc.getMedico().getNombre().equals(nombre))
                {
                    // si el id del medico es igual al que le pasamos por parametro lo guardamos en la lista
                   incidenciasmedico.add(inc);
                }
            }
        }
       catch(Exception e) 
        {
            throw new Exception("Error al obtener incidencias del medico "+ nombre+"  Error:" + e.getLocalizedMessage()); 
        }
       
  
       return incidenciasmedico;
    }
    
    public int obtenerNumeroPacientesenCola() throws Exception
    {
        int contador=0;
        List<Incidencia> incidencias = this.obtenerColaGraves();
        contador = incidencias.size();

    
        return contador;
    }
    
    public List<Incidencia> obtenerleveshistoricas() throws Exception
    {
     List<Incidencia> incidencias = null;
        List<Incidencia> leveshistoricas = new ArrayList();
        try
        {
            incidencias = incdao.getIncidencias();
            for (Incidencia inc : incidencias)
            {
                
                if(inc.getEstado().equals("HISTORICA") && inc.getTipo().equals("LEVE"))
                {
                    // si su estado es ACTIVA lo añadimos a la lista
                   leveshistoricas.add(inc);
                }
            }
        }
       catch(Exception e) 
        {
            throw new Exception("Error al obtener incidencias historicas y leves."); 
        }
        
 
        return leveshistoricas;
    }
    
    public List<Incidencia> obtenergraveshistoricas() throws Exception
    {
         List<Incidencia> incidencias = null;
        List<Incidencia> graveshistoricas = new ArrayList();
        try
        {
            incidencias = incdao.getIncidencias();
            for (Incidencia inc : incidencias)
            {
                
                if(inc.getEstado().equals("HISTORICA") && inc.getTipo().equals("GRAVE"))
                {
                    // si su estado es ACTIVA lo añadimos a la lista
                   graveshistoricas.add(inc);
                }
            }
        }
       catch(Exception e) 
        {
            throw new Exception("Error al obtener incidencias historicas y graves."); 
        }
        
 
        return graveshistoricas;
    }
    
    public long obtenerMediaCriticas() throws Exception
    {
        List<Incidencia> incidenciascriticas = new ArrayList();
        long media=0;
        int contador = 0;
        try
        {
            incidenciascriticas = this.obtenercriticashistoricas();
            for(Incidencia i: incidenciascriticas)
            {
                Date fecha1 = i.getFechaAtencion();
                Date fecha2 = i.getFechaSalida();
                media += (fecha2.getTime() - fecha1.getTime());
                contador++;
            }
            if(contador != 0)
            {
               media = media/contador;  
            }
            else
            {
                media = 0;
            }
        }
        catch(Exception e)
        {
            throw new Exception("Error al obtener la media de las incidencias criticas"+ e.getLocalizedMessage());
        }

        return media;
    }
    
    public long obtenerMediaLeves() throws Exception
    {
        List<Incidencia> incidenciasleves = new ArrayList();
        long media=0;
        int contador = 0;
        try
        {
            incidenciasleves = this.obtenerleveshistoricas();
            for(Incidencia i: incidenciasleves)
            {
                Date fecha1 = i.getFechaAtencion();
                Date fecha2 = i.getFechaSalida();
                media += (fecha2.getTime() - fecha1.getTime());
                contador++;
            }
            if(contador != 0)
            {
               media = media/contador;  
            }
            else
            {
                media = 0;
            }
        }
        catch(Exception e)
        {
            throw new Exception("Error al obtener la media de las incidencias leves"+ e.getLocalizedMessage());
        }
        

        return media;
    }
    public long obtenerMediaGraves() throws Exception
    {
        List<Incidencia> incidenciasgraves = new ArrayList();
        long media=0;
        int contador = 0;
        try
        {
            incidenciasgraves = this.obtenergraveshistoricas();
            for(Incidencia i: incidenciasgraves)
            {
                Date fecha1 = i.getFechaAtencion();
                Date fecha2 = i.getFechaSalida();
                media += (fecha2.getTime() - fecha1.getTime());
                contador++;
            }
            if(contador != 0)
            {
               media = media/contador;  
            }
            else
            {
                media = 0;
            }
        }
        catch(Exception e)
        {
            throw new Exception("Error al obtener la media de las incidencias graves"+ e.getLocalizedMessage());
        }
        

        return media;
    }

 //añado metodos para lo de persistencia...
 
}
