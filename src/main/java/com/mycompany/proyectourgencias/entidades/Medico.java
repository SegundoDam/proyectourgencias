/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.proyectourgencias.entidades;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 *
 * @author Marco Antonio
 */
@Entity
@Table(name="medicos")
public class Medico {
   
   enum EstadoMedico { DISPONIBLE, OCUPADO, FUERA_SERV,};
   @Id
   @GeneratedValue
   private int idMedico;
   private String nombre;
private EstadoMedico estado;
   
    public void setId(int id) {
        idMedico = id;
    }

    public void setNombre(String name) {
        nombre = name;
    }

    public void setEstado(String status) {
       if("DISPONIBLE".equals(status.toUpperCase()))
       {        
           estado = Medico.EstadoMedico.DISPONIBLE;
       }
       if("OCUPADO".equals(status.toUpperCase()))
       {        
           estado = Medico.EstadoMedico.OCUPADO;
       }
       if("FUERA_SERV".equals(status.toUpperCase()))
       {        
           estado = Medico.EstadoMedico.FUERA_SERV;
       }
    }

    public int getId() {
        return idMedico;
    }

    public String getNombre() {
        return nombre;
    }

    public String getEstado() {
        return estado.toString();
    }

    /*@Override
    public String toString() {
        return "Medico{" + "idMedico=" + idMedico + ", nombre=" + nombre + ", estado=" + estado + '}';
    }*/
   
}
