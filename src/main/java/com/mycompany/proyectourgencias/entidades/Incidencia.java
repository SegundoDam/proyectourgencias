/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.proyectourgencias.entidades;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**
 *
 * @author Marco Antonio
 */
@Entity
@Table(name="incidencias")
public class Incidencia implements Serializable 
{
//    private static final long serialVersionUID = 1L;

    public Incidencia()
    {
    }
   enum TipoIncidencia { LEVE, GRAVE, CRITICA,};
   enum EstadoIncidencia { ACTIVA, ESPERA, HISTORICA,};
   @Id
   @GeneratedValue
//   @Column(name="idIncidencia")
   private int idIncidencia;
  @Column(name="descripcion")
   private String descripcion;
   //@Temporal(TemporalType.DATE)
   private Date fechaEntrada;
  // @Temporal(TemporalType.DATE)
   private Date fechaAtencion;
   //@Temporal(TemporalType.DATE)
   private Date fechaSalida;
//   @Column(name="medico")
   @ManyToOne
   private Medico medico;
//   @Column(name="paciente")
   @ManyToOne
   private Paciente paciente;
//   @Column(name="tipo")
   public TipoIncidencia tipo;
//   @Column(name="estado")
   public EstadoIncidencia estado;
//   @Column(name="numEspera")
   private int numEspera;
   
   public Incidencia(Paciente paciente, String descripcion)
   {
       super();
//       Paciente pac = new Paciente();
//       this.paciente = pac.setIdPaciente();
       this.descripcion = descripcion;
   }

    public int getIdIncidencia() {
        return idIncidencia;
    }

    public void setIdIncidencia(int idIncidencia) {
        this.idIncidencia = idIncidencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaEntrada() {
        return fechaEntrada;
    }

    /*public void setFechaEntrada(String fechaEntrada) throws Exception {
        try
	{
            SimpleDateFormat sdf = new SimpleDateFormat ("dd-MM-yyyy");
		Date fecha = sdf.parse(fechaEntrada);
		this.fechaEntrada = fecha;
	}
	catch(ParseException e)
	{
		throw new Exception("Error en el parse de setFechaEntrada"+e.getLocalizedMessage());	
	}
        
    }*/
    public void setFechaEntrada(Date fechaEntrada){
        this.fechaEntrada=fechaEntrada;
    }
    public void setFechaAtencion(Date fechaAtencion){
        this.fechaAtencion=fechaAtencion;
    }

    /*public  String toString() {
        return "Incidencia{" + "idIncidencia=" + idIncidencia + ", descripcion=" + descripcion + ", fechaEntrada=" + fechaEntrada + ", fechaAtencion=" + fechaAtencion + ", fechaSalida=" + fechaSalida + ", medico=" + medico + ", paciente=" + paciente + ", tipo=" + tipo + ", estado=" + estado + ", numEspera=" + numEspera + '}';
    }*/

    public Date getFechaAtencion() {
        return fechaAtencion;
    }

   /* public void setFechaAtencion(String fechaAtencion) throws Exception {
        try
	{
            SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd");
		Date fecha = sdf.parse(fechaAtencion);
		this.fechaAtencion = fecha;
	}
	catch(ParseException e)
	{
		throw new Exception("Error en el parse de setFechaEntrada"+e.getLocalizedMessage());	
	}
    }*/

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) throws Exception {
        this.fechaSalida=fechaSalida;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public String getTipo() {
        return tipo.toString();
    }

    public  void setTipo(String tip) 
    {
       if("LEVE".equals(tip.toUpperCase()))
       {        
           tipo = Incidencia.TipoIncidencia.LEVE;
       }
       if("GRAVE".equals(tip.toUpperCase()))
       {        
           tipo = Incidencia.TipoIncidencia.GRAVE;
       }
       if("CRITICA".equals(tip.toUpperCase()))
       {        
           tipo = Incidencia.TipoIncidencia.CRITICA;
       }
    }

   

    public String getEstado() {
        return estado.toString();
    }

    public void setEstado(String status) 
    {
        if("ACTIVA".equals(status.toUpperCase()))
       {        
           estado = Incidencia.EstadoIncidencia.ACTIVA;
       }
       if("ESPERA".equals(status.toUpperCase()))
       {        
           estado = Incidencia.EstadoIncidencia.ESPERA;
       }
       if("HISTORICA".equals(status.toUpperCase()))
       {        
           estado = Incidencia.EstadoIncidencia.HISTORICA;
       }
    }

    public int getNumEspera() {
        return numEspera;
    }

    public void setNumEspera(int numEspera) {
        this.numEspera = numEspera;
    }
}
