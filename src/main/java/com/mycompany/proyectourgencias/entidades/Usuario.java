/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.proyectourgencias.entidades;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 *
 * @author Marco Antonio
 */
@Entity
@Table(name="usuarios")
public class Usuario implements Serializable 
{

    
    // esto que pongo aqui solo es para comprobacion del repositorio
    // no lo se seguro pero hecho en falta un método que me de el rol de un usuario 
    // y no se si iría aquí por eso no lo he puesto FRAN
   @Id
   @GeneratedValue
   private int id;
   private String nombre;
   private String usu;
   private String pwd;
   private TipoRol rol;
   
   
   // he incluido este método ya que lo precisaba para el UsuarioService FRAN
   public int getId() 
    {
        return id;
    }
  
   
   public void setNombre(String nom)
   {
       nombre=nom;
   }
   
   public String getNombre()
   {
       return nombre;
   }
   
   public void setUsu(String usuario)
   {
       usu=usuario;
   }
   
   public String getUsu()
   {
       return usu;
   }
   
   public void setPwd(String pass)
   {
       pwd=pass;
   }
   
   public String getPwd()
   {
       return pwd;
   }
   
   public void setRol(String tiporol)
   {
       if("ADMINISTRATIVO".equals(tiporol.toUpperCase()))
       {        
           rol = TipoRol.ADMINISTRATIVO;
       }
       if("SANITARIO".equals(tiporol.toUpperCase()))
       {        
           rol = TipoRol.SANITARIO;
       }
       if("DIRECTIVO".equals(tiporol.toUpperCase()))
       {        
           rol = TipoRol.DIRECTIVO;
       }
   }
   public TipoRol getRol()
   {
       return rol;
   }
   
   //Carmen-Añadido para pruebas a pelo **BORRAR**
    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", name=" + nombre + ", password=" + pwd + '}';
    }
   
   
}
