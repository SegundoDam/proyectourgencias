/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.proyectourgencias.servicio;

import com.mycompany.proyectourgencias.entidades.Usuario;

import java.util.List;
import org.junit.After;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author carmen
 * LOS TESTS SE HACEN SOBRE EL SERVICE POR LO QUE NO UTILIZAMOS LOS DAOS, SOLO LOS SERVICES AFECTADOS EN CADA CASO
 * CADA TESTS ES PARA PROBAR UNA UNICA COSA
 * LOS SERVICE PRIVADOS Y A NULL AL SER DECLARADOS
 */
public class UsuarioServiceTest
{

    private  UsuarioService sf = null;
    static String login = "root";
    static String password = "root";
    static String url = "jdbc:mysql://localhost/urgenciastest";

    static String deleteUsuarios = "DELETE FROM usuarios;";

    public UsuarioServiceTest()
    {
        sf=new UsuarioService();
    }

    /*@BeforeClass
    public static void setUpBeforeClass()
    {
        sf = new UsuarioService();
    }*/

    @After
    public void tearDown() throws Exception
    {
        Connection conn = null;
        PreparedStatement ps = null;
        try
        {
            conn = DriverManager.getConnection(url, login, password);
            ps = conn.prepareStatement(deleteUsuarios);
            ps.executeUpdate();
        }
        catch (Exception ex)
        {
            System.out.println("Ha habido un problema al iniciar los test"+ ex.getLocalizedMessage());
        }
        finally
        {
            try
            {
                ps.close();

                conn.close();
            }
            catch (SQLException ex)
            {
                throw new Exception("Error al cerrar la base de datos");
            }
        }
    }

    /**
     * Test of nuevoUsuario method, of class UsuarioService.
     *
     * @throws java.lang.Exception
     */
    @org.junit.Test
    public void testNuevoUsuario() throws Exception
    {
        System.out.println("nuevoUsuario");
        try
        {
            sf.nuevoUsuario("carmen", "pepec", "123456", "ADMINISTRATIVO");
        }
        catch (Exception e)
        {
            fail("TEST NO SUPERADO: No se ha introducido el nuevo usuario");
        }
    }

    @org.junit.Test
    public void testGetUsuario() throws Exception
    {
        System.out.println("getUsuario");
        try
        {
            sf.nuevoUsuario("Carmen", "csedano", "123456", "Administrativo");
            sf.getUsuario(1);

        }
        catch (Exception e)
        {
            fail("TEST NO SUPERADO: No se ha obtenido el usuario");
        }
    }
    /* Está comentado porque no puedo obtener el id
     @org.junit.Test
     public void testEliminarUsuario() throws Exception{
     System.out.println("EliminarUsuario");
     try{
     sf.nuevoUsuario("Carmen", "csedano", "123456", "Administrativo");
     sf.nuevoUsuario("Pepe", "pepito", "123456", "Administrativo");
     //arreglar
     sf.eliminarUsuario(1);
     //compruebo que hay un usuario
     List<Usuario> lista=sf.obtenerUsuarios();
     assertEquals(1,lista.size());
            
     }catch(Exception e){
     fail("TEST NO SUPERADO: No se ha eliminado correctamente el usuario");
     }
     }*/

    @org.junit.Test
    public void testObtenerUsuarios() throws Exception
    {
        try
        {
            sf.nuevoUsuario("Carmen", "csedano", "123456", "Administrativo");
            sf.nuevoUsuario("Pepe", "pepito", "123456", "Administrativo");
            List<Usuario> lista = sf.obtenerUsuarios();
            assertEquals(2, lista.size());
        }
        catch (Exception e)
        {
            fail("TEST NO SUPERADO: NO se han obtenido los usuarios");
        }
    }

    @org.junit.Test
    public void testActualizarUsuario() throws Exception
    {
        try
        {
            sf.actualizarUsuario(1, "Carmen", "csedano", "1234567", "Administrativo");
        }
        catch (Exception e)
        {
            fail("TEST NO SUPERADO: No se ha actualizado el usuario");
        }
    }

    @org.junit.Test
    public void testObtenerAdministradores() throws Exception
    {
        try
        {
            sf.nuevoUsuario("Carmen", "csedano", "123456", "Administrativo");
            sf.nuevoUsuario("Alberto", "Acomido", "1234356", "Sanitario");
            List<Usuario> u = sf.obtenerUsuariosAdministradores();
            assertEquals(1, u.size());
        }
        catch (Exception e)
        {
            fail("TEST NO SUPERADO: No se han obtenido los usuarios administradores");
        }
    }

}
