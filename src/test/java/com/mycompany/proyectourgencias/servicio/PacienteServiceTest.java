/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.proyectourgencias.servicio;

import com.mycompany.proyectourgencias.entidades.Incidencia;
import com.mycompany.proyectourgencias.entidades.Paciente;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.List;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ruben
 * LOS TESTS SE HACEN SOBRE EL SERVICE POR LO QUE NO UTILIZAMOS LOS DAOS, SOLO LOS SERVICES AFECTADOS EN CADA CASO
 * CADA TESTS ES PARA PROBAR UNA UNICA COSA
 * LOS SERVICE PRIVADOS Y A NULL AL SER DECLARADOS
 */
public class PacienteServiceTest
{

    static PacienteService pS = null;

    static SessionFactory sessionFactory;
    static String bd="urgenciastest";
    static String login="root";
    static String password="root";
    static String url="jdbc:mysql://localhost/urgenciastest";

   static String deletePacientes = "DELETE FROM pacientes;";

  

    public PacienteServiceTest()
    {
    }

 @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
        pS = new PacienteService();
       
    }
   

    @Before
    public void setUp() throws Exception
    {
        pS = new PacienteService();
        
   
        //introduzco dos pacientes nuevos
        
        pS.nuevoPaciente("Pepico22", "01/88329244/21X");
        pS.nuevoPaciente("Fran22", "02/32551321/32Y");
        
        //Paciente p=pS.obtenerPacienteNumSegSocial("01/88329244/21X");
     
        
     
    }

    @After
    public void tearDown() throws Exception
    {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        PreparedStatement preparedStatement2 = null;
        PreparedStatement preparedStatement3 = null;
        
       

        try
        {
            
            conn = DriverManager.getConnection(url, login, password);

            preparedStatement = conn.prepareStatement(deletePacientes);
            preparedStatement.executeUpdate();
            

            
        }
        catch (Exception e)
        {
            throw new Exception("No se ha podido realizar el test: ", e);
        }
        finally
        {
            try
            { 
          
                preparedStatement.close();         
            
                conn.close();
            }
            catch (Exception e)
            {
                throw new Exception("No se ha podido cerrar la base de datos: ", e);
            }

        }
    }

    /**
     * Test of nuevoPaciente method, of class PacienteService.
     */
    @Test
    public void testNuevoPaciente() throws Exception
    {
        try
        {
            // Creamos los pacientes
            /*pS.nuevoPaciente("Jorge", "01/88329244/21X");
             pS.nuevoPaciente("Fran", "02/32551321/32Y");
             pS.nuevoPaciente("Carmen", "03/34326253/74B");
             pS.nuevoPaciente("Ruben", "04/00788027/33C");*/

            List<Paciente> listaPacientes = pS.obtenerTodosPaciente();

            // Comprobamos que se han obtenido todos los pacientes creados
            if (listaPacientes.size() != 2)
            {
                fail("No se han registrado todos los Pacientes");
            }
            //Cuando se añaden los pacientes en un principio no tienen inidencia...
           /* for (Paciente p : listaPacientes) {
             // Comprobamos que ningun campo de cada paciente esta a null
             assertNotNull(p.getIdPaciente());
             assertNotNull(p.getIncidencias());
             assertNotNull(p.getNombre());
             assertNotNull(p.getnSegSoc());
             }*/
        }
        catch (Exception e)
        {
            throw new Exception("TEST NO SUPERADO. No se ha podido insertar los pacientes ya que hay algun campo a null", e);
        }
    }

    /**
     * Test of actualizarPaciente method, of class PacienteService.
     */
    @Test
    public void testActualizarPaciente() throws Exception
    {
        try
        {
//            Paciente p = pdao.getPacienteSeguridad("01/88329244/21X");
            Paciente p = pS.obtenerPacienteNumSegSocial("01/88329244/21X");

            //p.setNombre("Saul");
            pS.actualizarPaciente(p.getIdPaciente(), "Saul", p.getnSegSoc());
              Paciente p1 = pS.obtenerPacienteNumSegSocial("01/88329244/21X");
         
            if (!(p1.getNombre().equals("Saul") && p1.getnSegSoc().equals("01/88329244/21X")))
            {
                fail("No se ha podido actualizar el paciente");
            }

        }
        catch (Exception e)
        {
            throw new Exception("TEST NO SUPERADO. Se ha producido una excepcion de tipo : " + e.getLocalizedMessage());
        }
    }

    /**
     * Test of eliminarPaciente method, of class PacienteService.
     */
    @Test
    public void testEliminarPaciente() throws Exception
    {
        try
        {
            //Paciente p = pS.getIdao().getPacienteSeguridad("01/88329244/21X");
           Paciente p = pS.obtenerPacienteNumSegSocial("01/88329244/21X");
            pS.eliminarPaciente(p.getIdPaciente());

            List<Paciente> listPacientes = pS.obtenerTodosPaciente();
            assertEquals(1, listPacientes.size());
          /*  if (listPacientes.size() != 1)
            {
                fail("No se ha podido eliminar el paciente");
            }*/

            /* for (Paciente p : listPacientes) {
             assertNotNull(p.getIdPaciente());
             assertNotNull(p.getIncidencias());
             assertNotNull(p.getNombre());
             assertNotNull(p.getnSegSoc());
             }*/
        }
        catch (Exception e)
        {
            throw new Exception("TEST NO SUPERADO. Se ha producido una excepcion de tipo: " + e.getLocalizedMessage());
        }
    }

    /**
     * Test of obtenerPaciente method, of class PacienteService.
     */
    @Test
    public void testObtenerPaciente() throws Exception
    {
        try
        {
            Paciente p = pS.obtenerPacienteNumSegSocial("01/88329244/21X");
            Paciente p1 = pS.obtenerPacienteNumSegSocial("01/88329244/21X");
            
//            Paciente pObt = pS.obtenerPaciente(p.getIdPaciente());
//            Paciente pObt1 = pS.obtenerPaciente(p1.getIdPaciente());

            if (p == null && p1 == null)
            {
                fail("No se ha obtenido el paciente con el número de Seg.Social especificado. Paciente no Existe");
            }
        }
        catch (Exception e)
        {
            throw new Exception("TEST NO SUPERADO. Se ha producido una excepcion de tipo: " + e.getLocalizedMessage());
        }
    }

    /**
     * Test of obtenerTodosPaciente method, of class PacienteService.
     */
    @Test
    public void testObtenerTodosPaciente() throws Exception
    {
        try
        {
            List<Paciente> listPacientes = pS.obtenerTodosPaciente();

            if (listPacientes.size() != 2)
            {
                fail("No se han obtenido todos los pacientes");
            }

            /*for (Paciente p : listPacientes) {
             assertNotNull(p.getIdPaciente());
             assertNotNull(p.getIncidencias());
             assertNotNull(p.getNombre());
             assertNotNull(p.getnSegSoc());
             }*/
        }
        catch (Exception e)
        {
            throw new Exception("TEST NO SUPERADO. Se ha producido una excepcion de tipo: " + e.getLocalizedMessage());

        }
    }
}
