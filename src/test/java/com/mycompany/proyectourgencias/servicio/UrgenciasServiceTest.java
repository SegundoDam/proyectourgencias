/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.proyectourgencias.servicio;

import com.mycompany.proyectourgencias.datos.IncidenciaDAO;
import com.mycompany.proyectourgencias.datos.MedicoDAO;
import com.mycompany.proyectourgencias.datos.PacienteDAO;
import com.mycompany.proyectourgencias.entidades.Incidencia;
import com.mycompany.proyectourgencias.entidades.Medico;
import com.mycompany.proyectourgencias.entidades.Paciente;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static junit.framework.Assert.fail;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author carmen
 */
public class UrgenciasServiceTest {
     static UrgenciasService us=null;
     static PacienteService pas=null;
     static PacienteDAO daopa=null;
     static MedicoService ms=null;
     static MedicoDAO md=null;
     static IncidenciaDAO ida=null;
    static SessionFactory sessionFactory;
    static String bd="urgenciastest";
    static String login="root";
    static String password="root";
    static String url="jdbc:mysql://localhost/urgenciastest";
   static String deleteIncidencias = "DELETE FROM incidencias;";
   static String deleteIncidencias2 = "DELETE FROM pacientes_incidencias;";
   static String deleteMedicos = "DELETE FROM medicos;";
   static String deletePacientes = "DELETE FROM pacientes;";
   // static int id;

    public   UrgenciasServiceTest(){
       
    }
    
   
    @BeforeClass
   public static void setUpBeforeClass() throws Exception {
        us=new UrgenciasService();
        pas=new PacienteService();
     
        ms=new MedicoService();
        md=new MedicoDAO();
        ida=new IncidenciaDAO();
       

            
        
        
    }
    
    @AfterClass
    public static void tearDownClass() {
        
    }
    
    @Before
    public void setUp() throws Exception {
        //ps.nuevoPaciente(bd, bd);
         daopa=new PacienteDAO();
         us=new UrgenciasService();
        ms=new MedicoService();
        md=new MedicoDAO();
        ida=new IncidenciaDAO();
       


         
       Paciente p=new Paciente();
        p.setNombre("carmen");
        p.setnSegSoc("12345");
        daopa.addPaciente(p);
         Paciente p1=new Paciente();
        p1.setNombre("Pepita");
        p1.setnSegSoc("12333");
        daopa.addPaciente(p1);
        Paciente p2=new Paciente();
        p2.setNombre("Lola");
        p2.setnSegSoc("22221");
        daopa.addPaciente(p2);
        Paciente p3=new Paciente();
        p3.setNombre("Clemente");
        p3.setnSegSoc("12221");
        daopa.addPaciente(p3);
        
        ms.nuevoMedico("Pepe");
        ms.nuevoMedico("Alberto");
         ms.nuevoMedico("Jorge");
         ms.nuevoMedico("Paco");
         
       Medico m=md.getMedicoNombre("Pepe");
       Medico m1=md.getMedicoNombre("Alberto");
       Medico m2=md.getMedicoNombre("Jorge");
       ms.actualizarMedico(m1.getId(), "Alberto", "DISPONIBLE");
       ms.actualizarMedico(m2.getId(), "Jorge", "DISPONIBLE");
   
         
       
   
       
        
    }
    
    @After
    public void tearDown() throws Exception {
        Connection conn = null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
                PreparedStatement ps3 = null;
                PreparedStatement ps4 = null;

		try {
			
                        daopa.finalizar();
                        md.finalizar();
			conn = DriverManager.getConnection(url, login, password);

			ps = conn.prepareStatement(deleteIncidencias);
			ps.executeUpdate();
			ps2 = conn.prepareStatement(deleteMedicos);
			ps2.executeUpdate();
                        ps3 = conn.prepareStatement(deletePacientes);
			ps3.executeUpdate();
                        ps4 = conn.prepareStatement(deleteIncidencias2);
			ps4.executeUpdate();

		} catch (Exception ex) {

			System.out.println("Ha habido un problema al iniciar los test"
					+ ex.getLocalizedMessage());
		} finally {
			try {
				ps.close();
				ps2.close();
                                ps3.close();
                                ps4.close();
				conn.close();
			} catch (SQLException ex) {
				throw new Exception("Error al cerrar la base de datos");
			}

		}

    }

   
    @org.junit.Test
    public void testRegistrarIncidenciaLeve() throws Exception {
        System.out.println("registrarIncidenciaLeve");
        try{
            Paciente p;
            p=daopa.getPacienteSeguridad("12345");
            int id2=p.getIdPaciente();
            us.registrarIncidenciaLeve(id2, "Dolor muelas");
            
            List<Incidencia> lista=us.obtenerColaLeves();
            //se espera 0 incidencia LEVE
            assertEquals(0, lista.size());
            
            //en la base de datos hay 3 medicos, dos disponibles y el otro fuera de servicio
            //como hemos registrado la incidencia el medico debe haber pasado a ocupado, se esperan 1 medico disponible
            List<Medico> listamedico=ms.obtenerMedicosServicio();
            assertEquals(1,listamedico.size());
            
            
        }catch(Exception e){
            fail ("TEST NO SUPERADO: No se ha podido registrar la incidencia leve");
        }
    }

    /**
     * Test of registrarIncidenciaGrave method, of class UrgenciasService.
     * @throws java.lang.Exception
     */
    @org.junit.Test
    public void testRegistrarIncidenciaGrave() throws Exception {
  
        try{
           Paciente pp=new Paciente();
           pp.setNombre("Aurora");
           pp.setnSegSoc("11111");
           pas.nuevoPaciente("Aurora", "11111");
            Paciente p1;
            pp=daopa.getPacienteSeguridad("11111");
            int id2=pp.getIdPaciente();
            us.registrarIncidenciaGrave(id2, "hola");
            
            List<Incidencia> lista=us.obtenerColaGraves();
            //se espera 0 incidencia Grave
            assertEquals(0, lista.size());
             //en la base de datos hay 3 medicos, dos disponibles y el otro fuera de servicio
            //como hemos registrado la incidencia el medico debe haber pasado a ocupado, se esperan 1 medico disponible
            List<Medico> listamedico=ms.obtenerMedicosServicio();
            assertEquals(1,listamedico.size());
            
        }catch(Exception e){
            fail("TEST NO SUPERADO: No se ha podido registrar la incidencias Grave "+e.getLocalizedMessage());
        }
    }

    /**
     * Test of registrarIncidenciaCritica method, of class UrgenciasService.
     */
    
    public void testRegistrarIncidenciaCritica() throws Exception {
        try{
         Paciente p = daopa.getPacienteSeguridad("12345");
            int id2=p.getIdPaciente();
            us.registrarIncidenciaCritica(id2, "Dolor muelas");
            
            List<Incidencia> lista2=ida.getIncidencias();
            List<Incidencia> graves=new ArrayList();
            Incidencia i1=null;
            for(Incidencia i: lista2){
                if(i.getEstado().equals("CRITICA")){
                    i1=i;
                  }
            }
            graves.add(i1);
           if(graves.size() != 1){
               fail("No se ha registrado correctamente la incidencia Critica");
           }  
            
            //en la base de datos hay 3 medicos, dos disponibles y el otro fuera de servicio
            //como hemos registrado la incidencia el medico debe haber pasado a ocupado, se esperan 1 medico disponible
            List<Medico> listamedico=ms.obtenerMedicosServicio();
            assertEquals(1,listamedico.size());
           
        }catch(Exception e){
            fail("TEST NO SUPERADO: Error al registrar incidencia Critica");
        }
    }
    
     @org.junit.Test
    public void TestObtenerColasLeves() throws Exception{
        //registro dos incidencias
        try{
            Paciente p = daopa.getPacienteSeguridad("12345");
            Paciente p1=daopa.getPacienteSeguridad("12333");
            Paciente p2=daopa.getPacienteSeguridad("22221");
             int id1=p.getIdPaciente();
             int id2=p1.getIdPaciente();
             int id3=p2.getIdPaciente();
            //registro tres incidencias
            us.registrarIncidenciaLeve(id1, "Dolor muelas");
            us.registrarIncidenciaLeve(id2, "Locura Transitoria");
            us.registrarIncidenciaLeve(id3, "Dolor Cabeza");
            //obtengo los medicos en servicio
            List<Medico> listaDisp=ms.obtenerMedicosServicio();
            //espero que no haya ningun medico disponible
            assertEquals(0, listaDisp.size());
            //Obtengo la lista de colaLeves
            List<Incidencia> listaLeves=us.obtenerColaLeves();
            //Espero tener una incidencia en la lista
            assertEquals(1, listaLeves.size());
        }catch(Exception e){
            fail("TEST NO SUPERADO:Error al obtener la cola de leves");
        }
           
        
    }
    
 @org.junit.Test
    public void TestObtenerColasGraves() throws Exception{
        //registro dos incidencias
        try{
            Paciente p = daopa.getPacienteSeguridad("12345");
            Paciente p1=daopa.getPacienteSeguridad("12333");
            Paciente p2=daopa.getPacienteSeguridad("22221");
             int id1=p.getIdPaciente();
             int id2=p1.getIdPaciente();
             int id3=p2.getIdPaciente();
            //registro tres incidencias
            us.registrarIncidenciaGrave(id1, "Dolor muelas");
            us.registrarIncidenciaGrave(id2, "Locura Transitoria");
            us.registrarIncidenciaGrave(id3, "Dolor Cabeza");
            //obtengo los medicos en servicio
            List<Medico> listaDisp=ms.obtenerMedicosServicio();
            //espero que no haya ningun medico disponible
            assertEquals(0, listaDisp.size());
            //Obtengo la lista de colaLeves
            List<Incidencia> listaGraves=us.obtenerColaGraves();
            //Espero tener una incidencia en la lista
            assertEquals(1, listaGraves.size());
        }catch(Exception e){
            fail("TEST NO SUPERADO:Error al obtener la cola de leves");
        }
           
        
    }
    /**
     * Test of finalizarIncidencia method, of class UrgenciasService.
     */
     @org.junit.Test
    public void testFinalizarIncidencia() throws Exception {
        try{//registro tres incidencia
         Paciente p = daopa.getPacienteSeguridad("12345");
            Paciente p1=daopa.getPacienteSeguridad("12333");
            Paciente p2=daopa.getPacienteSeguridad("22221");
             int id1=p.getIdPaciente();
             int id2=p1.getIdPaciente();
             int id3=p2.getIdPaciente();
            //registro tres incidencias
            us.registrarIncidenciaGrave(id1, "Dolor muelas");
            us.registrarIncidenciaGrave(id2, "Locura Transitoria");
            us.registrarIncidenciaGrave(id3, "Dolor Cabeza");
            //una de ellas pasara a la cola de graves
            //obtengo la lista de incidencias activas
            List<Incidencia> listaActivas=us.obtenerActivas();
            //espero dos activas
            assertEquals(2, listaActivas.size());
            //recorro la lista de activas para finalizar una de ellas
            Incidencia i1=null;
            for(Incidencia i: listaActivas){
                    i1=i;
                break;
            }
            //finalizo la incidencia
            us.finalizarIncidencia(i1.getIdIncidencia());
            //obtengo de nuevo las incidencias activas
            List<Incidencia> listaActivas2=us.obtenerActivas();
            //espero 1 incidencia activa al finalizar una de ellas
            assertEquals(2, listaActivas2.size());
            //obtengo las incidencias graves. Habia una en la cola y ahora no debe haber ninguna
            //ya que se le asigna el medico disponible
            List<Incidencia> listaGraves=us.obtenerColaGraves();
            assertEquals(0, listaGraves.size());
            //compruebo cuantos medicos disponibles hay debe haber 0
            List<Medico> medDisponibles=ms.obtenerMedicosServicio();
            assertEquals(0, medDisponibles.size());
          
        }catch(Exception e){
            fail("TEST NO SUPERADO: Error al finalizar incidencia ");
        }
        
    }
    
    @Test
    public void testComenzarTurno(){
        try{
         //antes de empezar los test tenemos un medico fuera de servicio
         //llamado Jorge
          Medico m2=md.getMedicoNombre("Pepe");
            us.comenzarTurno(m2.getId());
         //al comenzar el turno su estado pasa a ser disponible
         //compruebo que en este caso hay 3 medicos disponibles
          List<Medico> lista=ms.obtenerMedicosServicio();
            assertEquals(3, lista.size());
            
        }catch(Exception e){
            fail("TEST NO SUPERADO: No se ha iniciado el turno correctamente");
        }
    }
    
    @Test
    public void testfinalizarTurno(){
        try{
             Paciente p = daopa.getPacienteSeguridad("12345");
            Paciente p1=daopa.getPacienteSeguridad("12333");
            Paciente p2=daopa.getPacienteSeguridad("22221");
             int id1=p.getIdPaciente();
             int id2=p1.getIdPaciente();
             int id3=p2.getIdPaciente();
            //registro tres incidencias
            us.registrarIncidenciaGrave(id1, "Dolor muelas");
            us.registrarIncidenciaGrave(id2, "Locura Transitoria");
            us.registrarIncidenciaGrave(id3, "Dolor Cabeza");
             List<Incidencia> listaActivas=us.obtenerActivas();
            //espero dos activas
            assertEquals(2, listaActivas.size());
            //recorro la lista de activas para finalizar una de ellas
            Incidencia i1=null;
            for(Incidencia i: listaActivas){
                    i1=i;
                break;
            }
            //finalizo la incidencia
            us.finalizarIncidencia(i1.getIdIncidencia());
            //compruebo que el estado del medico que ha atendido la incidencia esta fuera de servicio
            Medico m1=i1.getMedico();
            if(m1.getEstado().equals("FUERA_SERV")){
                fail("Error: El estado del medico no ha sido actualizado a FUERA_SERV");
            }
            
                
        }catch(Exception e){
            fail("TEST NO SUPERADO: No se ha finalizado el turno correctamente");
        }
    }
    @Test
    public void TestfinalizarTurno() throws Exception{
         //antes de empezar los test tenemos dos medicos disponible
         //finalizo el turno de uno de ellos
          Medico m2=md.getMedicoNombre("Alberto");
            us.finalizarTurno(m2.getId());
         //su estado pasa a fuera de servicio
         //compruebo que en este caso hay 1 medico disponible
          List<Medico> lista=ms.obtenerMedicosServicio();
            assertEquals(1, lista.size());
        
    }
   
    @Test
    public void TestCriticaSinMedicoDisponible() throws Exception{
       try{//añado 3 pacientes
       Paciente p = daopa.getPacienteSeguridad("12345");
       Paciente p1=daopa.getPacienteSeguridad("12333");
       Paciente p2=daopa.getPacienteSeguridad("22221");
        Paciente p3=daopa.getPacienteSeguridad("12221");

       
       int id1=p.getIdPaciente();
       int id2=p1.getIdPaciente();
       int id3=p2.getIdPaciente();
       int id4=p2.getIdPaciente();

            //registro 3 incidencias leves una de ellas ira a la cola
            us.registrarIncidenciaLeve(id1, "Dolor muelas");
            us.registrarIncidenciaLeve(id2, "Locura Transitoria");
            us.registrarIncidenciaLeve(id3, "Paciente Hipocondriaco"); //pasa a la cola
            //compruebo que hay una incidencia en la cola
            List<Incidencia> levesActual=us.obtenerColaLeves();
            assertEquals(1, levesActual.size());
            //registro la incidencia critica
            us.registrarIncidenciaCritica(id4, "Se muere");
            //como no hay medicos disponibles compruebo que en la cola hay dos incidencias leves
            //ya que se le asigna medico y pasa a la cola de leves
            List<Incidencia> leve=us.obtenerColaLeves();
            assertEquals(2, leve.size());
          
       }catch(Exception e){
           fail("Test no superado: No se ha podido registrar la incidencia critica sin medico");
       }
            
   }
    
    
}
